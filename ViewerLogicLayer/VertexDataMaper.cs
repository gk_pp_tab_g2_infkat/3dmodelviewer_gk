using ObjLoader.Loader.Data.Elements;
using OpenTK.Mathematics;
using Vertex = ModelRenderEngine.Vertex;
using VertexObj = ObjLoader.Loader.Data.VertexData;

namespace ViewerLogicLayer
{
    /// <summary>
    /// Klasa do mapowania danych wierzcholkow z loadera plikow obj do typu danych dla renderera
    /// (TOMEK STEFANIAK) JESLI COS NIE DZIALA TO POPRAW
    /// </summary>
    public static class VertexDataMapper
    {
        /// <summary>
        /// Mapuje pojedynczy wierzcholek
        /// </summary>
        /// <param name="pos">wektor pozycji</param>
        /// <param name="normal">wektor normalnych</param>
        /// <param name="texture">wektor tekstur</param>
        /// <returns>Wierzcholek typu ModelRenderEngine.Vertex</returns>
        public static Vertex MapVertex(VertexObj.Vertex pos, VertexObj.Normal normal, VertexObj.Texture texture )
        {
            return new Vertex(
                new Vector3(pos.X, pos.Y, pos.Z),
                new Vector3(normal.X, normal.Y, normal.Z),
                new Vector2(texture.X, texture.Y)
                );
        }

        /// <summary>
        /// Mapuje wszystkie wierzcholki do listy wierzcholkow typu ModelRenderEngine.Vertex
        /// </summary>
        /// <param name="verts">Lista wektorow pozycji</param>
        /// <param name="normals">lista wektorow normlanych</param>
        /// <param name="textures">lista wektorow map tekstur</param>
        /// <returns>Liste typu Vertex</returns>
        public static List<Vertex> MapVertices(
            IEnumerable<VertexObj.Vertex> verts,
            IEnumerable<VertexObj.Normal> normals,
            IEnumerable<VertexObj.Texture> textures)
        {
            if (!textures.Any())
                textures = VertexMapperMathHelper.CalculateTextures(verts);
            
            if (normals.Count() == textures.Count() && normals.Count() == verts.Count() && verts.Count() > 0)
            {
                return verts.ZipThree(normals, textures, MapVertex).ToList();
            }

            if (normals.Count() == 0 && verts.Count() == textures.Count() && verts.Count() > 0)
            {
                return verts.Zip(textures, (v, t) => 
                    new Vertex(new Vector3(v.X, v.Y, v.Z), null, new Vector2(t.X, t.Y))
                ).ToList();
            }

            if (textures.Count() == 0 && verts.Count() == normals.Count() && verts.Count() > 0)
            {
                return verts.Zip(normals, (v, n) => 
                    new Vertex(new Vector3(v.X, v.Y, v.Z), new Vector3(n.X, n.Y, n.Z), null)
                ).ToList();
            }

            if (verts.Count() != 0)
            {
                var vertices =
                    from v in verts
                    select new Vertex(new Vector3(v.X, v.Y, v.Z), null, null);
                return vertices.ToList();
            }

            return null;
        }

        public static uint[] MapIndices(IList<Face> faces)
        {
            var indices = new uint[faces.Count * 3];
            int i = 0;
            foreach (var idx in faces)
            {
                Array.Copy(new uint[]
                {
                    Convert.ToUInt32(idx[0].VertexIndex - 1), 
                    Convert.ToUInt32(idx[1].VertexIndex - 1), 
                    Convert.ToUInt32(idx[2].VertexIndex - 1)
                }, 0, indices, i, 3);
                i += 3;
            }
            return indices;
        }
    }

    /// <summary>
    /// Klasa rozszerzajaca interface IEnumerable o metode Zip dla 3 różnych list
    /// </summary>
    public static class ZipExtention
    {
        /// <summary>
        /// Metoda pozwalająca zapamować 3 różne listy do 1 konkretnej
        /// </summary>
        /// <param name="source">1 lista</param>
        /// <param name="second">2 lista</param>
        /// <param name="third">3 lista</param>
        /// <param name="func">funkcja mapujaca listy</param>
        /// <typeparam name="T1">typ 1 listy</typeparam>
        /// <typeparam name="T2">typ 2 listy</typeparam>
        /// <typeparam name="T3">typ 3 listy</typeparam>
        /// <typeparam name="TResult">Typ zwracany dla listy</typeparam>
        /// <returns>Nowa liste o type TResult</returns>
        public static IEnumerable<TResult> ZipThree<T1, T2, T3, TResult>(
            this IEnumerable<T1> source,
            IEnumerable<T2> second,
            IEnumerable<T3> third,
            Func<T1, T2, T3, TResult> func)
        {
            using (var e1 = source.GetEnumerator())
            using (var e2 = second.GetEnumerator())
            using (var e3 = third.GetEnumerator())
            {
                while (e1.MoveNext() && e2.MoveNext() && e3.MoveNext())
                    yield return func(e1.Current, e2.Current, e3.Current);
            }
        }
    }
}

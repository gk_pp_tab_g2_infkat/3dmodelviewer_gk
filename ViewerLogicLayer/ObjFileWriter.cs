using ModelRenderEngine;
using OpenTK.Mathematics;

namespace ViewerLogicLayer
{
    /// <summary>
    /// Klasa zapisujaca model 3d do pliku obj
    /// </summary>
    public class ObjFileWriter
    {
        /// <summary>
        /// Pole przechowujące obiekt strumienia do plików 
        /// </summary>
        private readonly StreamWriter _fileStreamWriter;
        /// <summary>
        /// Konstruktor z parametrem strumienia do zapisu do pliku (Obiekt ten nie jest automatycznie zwalnianym, należy ręcznie to zrobić)
        /// </summary>
        /// <param name="fileStreamWriter">Obiekt strumienia do pliku</param>
        /// <example>
        /// Przykład użycia klasy ObjFileWriter z automatycznym zwalnianiem zasobu
        /// <code>
        /// using (var fileWriter = new StreamWriter(File.OpenWrite(path)))
        ///{
        ///    var objWriter = new ObjFileWriter(fileWriter);
        ///    objWriter.WriteFile(verts, indices);
        ///}
        /// </code>
        /// </example>
        public ObjFileWriter(StreamWriter fileStreamWriter)
        {
            _fileStreamWriter = fileStreamWriter;
        }
        /// <summary>
        /// Zapis do podanego pliku modelu do formatu obj z tablic wierzchołków i indeksów
        /// </summary>
        /// <param name="verticesArray">Obiekt tablicy wierzchołówk</param>
        /// <param name="indices">Tablica indeksów</param>
        public void WriteFile(VerticesArray verticesArray, uint[] indices)
        {
            var objFilePos = new List<string>();
            var objFileNorm = new List<string>();
            var objFileTmap = new List<string>();
            var objFileText = new List<string>();
    
            foreach (var vert in verticesArray.Vertices)
            {
                objFilePos.Add($"v {vert.Position.X} {vert.Position.Y} {vert.Position.Z}");
                if (vert.Normal is Vector3 norm)
                    objFileNorm.Add($"vn {norm.X} {norm.Y} {norm.Z}");
                if (vert.TextureMapping is Vector2 text)
                    objFileTmap.Add($"vt {text.X} {text.Y}");
            }
            
            objFileText.AddRange(objFilePos);
            objFileText.AddRange(objFileNorm);
            objFileText.AddRange(objFileTmap);
    
            for (var i = 0; i < indices.Length; i += 3)
            {
                objFileText.Add($"f {indices[i]+1} {indices[i+1]+1} {indices[i+2]+1}");
            }
            
            foreach (var line in objFileText)
            {
                _fileStreamWriter.WriteLine(line);
            }
        }
        /// <summary>
        /// Asynchroniczna wersja metody WriteFile
        /// </summary>
        /// <param name="verticesArray">Obiekt tablicy wierzchołówk</param>
        /// <param name="indices">Tablica indeksów</param>
        public async Task WriteFileAsync(VerticesArray verticesArray, uint[] indices)
        {
            await Task.Run(() => WriteFile(verticesArray, indices));
        }
        
        
    }
}


namespace ViewerLogicLayer
{
    /// <summary>
    /// Enum reprezentujacy rodzaj wywolywanego typu renderowania
    /// </summary>
    public enum DisplayMethod
    {
        Wireframe = 0,
        Standard = 1,
        Textured = 2
    }
}


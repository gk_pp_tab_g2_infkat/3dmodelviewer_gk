using ModelRenderEngine;
using ObjLoader.Loader.Data.Elements;
using OpenTK.Mathematics;
using Vertex = ModelRenderEngine.Vertex;
using VertexObj = ObjLoader.Loader.Data.VertexData;

namespace ViewerLogicLayer
{
    
    /// <summary>
    /// Klasa statyczna z metodami pomocniczymi do obliczan na wierzcholka z parsera plikow OBJ
    /// </summary>
    public static class VertexMapperMathHelper
    {
        public static IEnumerable<VertexObj.Texture> CalculateTextures(IEnumerable<VertexObj.Vertex> positionVertices)
        {
            var texts = new List<VertexObj.Texture>();

            foreach (var vert in positionVertices)
            {
                var normalisedVertex = VertexMapperMathHelper.Normalize(vert);
                texts.Add(new VertexObj.Texture(normalisedVertex.X, normalisedVertex.Y));
            }

            return texts;
        }

        public static void CaculateVetexNormals(VerticesArray verticesArray, uint[] faceIndices)
        {
            for (int i = 0; i < faceIndices.Length; i += 3)
            {
                Vector3 A = verticesArray.Vertices[(int) faceIndices[i]].Position;
                Vector3 B = verticesArray.Vertices[(int) faceIndices[i+1]].Position;
                Vector3 C = verticesArray.Vertices[(int) faceIndices[i+2]].Position;

                Vector3 AB = B - A;
                Vector3 AC = C - A;
                Vector3 ABxAC = Vector3.Cross(AB, AC);
            
                verticesArray.Vertices[(int) faceIndices[i]].Normal = new Vector3(Vector3.Zero + ABxAC);
                verticesArray.Vertices[(int) faceIndices[i+1]].Normal = new Vector3(Vector3.Zero + ABxAC);
                verticesArray.Vertices[(int) faceIndices[i+2]].Normal = new Vector3(Vector3.Zero + ABxAC);

            }
        }

        public static VertexObj.Vertex Normalize(VertexObj.Vertex vertex)
        {
            var vecLength = (float) Math.Sqrt((Math.Pow(vertex.X, 2)) + (Math.Pow(vertex.Y, 2)) + (Math.Pow(vertex.Z, 2)));

            return new VertexObj.Vertex(
                vertex.X / vecLength,
                vertex.Y / vecLength,
                vertex.Z / vecLength
            );
        }
    }
}

﻿
using Avalonia.OpenGL;
using JetBrains.Annotations;
using ModelRenderEngine;
using ObjLoader;
using ObjLoader.Loader.Data;
using ObjLoader.Loader.Data.Elements;
using ObjLoader.Loader.Data.VertexData;
using ObjLoader.Loader.Loaders;
using Texture = ObjLoader.Loader.Data.VertexData.Texture;
using Vertex = ObjLoader.Loader.Data.VertexData.Vertex;
using VertexMRE = ModelRenderEngine.Vertex;
using static Avalonia.OpenGL.GlConsts;
using OpenTK.Mathematics;

namespace ViewerLogicLayer
{
    /// <summary>
    /// Klasa reprezentująca obiekt modelu 3D, pozwalająca na ładowanie go, zmianę jego parametrów i renderowanie go w widoku OpenGl
    /// </summary>
    public class ModelObject
    {
        /// <summary>
        /// Właściwość mówiąca czy model został załadowany
        /// </summary>
        public bool IsLoaded { get; set; }
        /// <summary>
        /// Właściwość mówiąca czy do renderowania używane jest światło
        /// </summary>
        public bool UsesLight { get; set; }
        /// <summary>
        /// Właściwość mówiące czy do renderowania jest używana tekstura
        /// </summary>
        public bool UsesTexture { get; set; }
        /// <summary>
        /// Pole przechowujące wierzchołki to do przekazania do renderera opengl
        /// </summary>
        private float[] verticesGl;
        /// <summary>
        /// Pole zawierające indeksy do index buffer
        /// </summary>
        private uint[] indices;
        /// <summary>
        /// pole bufora indeksów
        /// </summary>
        private IndexBuffer ibo;
        /// <summary>
        /// Pole przechowujące informację o tablicy wierzchołków w karcie graficznej
        /// </summary>
        private VertexArray va;
        /// <summary>
        /// Obiekt przechowujacy informację o buforze wierzchołków
        /// </summary>
        private VertexBuffer vbo;
        /// <summary>
        /// Pole przechowujące obiekt renderera
        /// </summary>
        private Renderer renderer;
        /// <summary>
        /// Włąsciwość prezentująca aktualny tryb wyświetlania modelu
        /// </summary>
        public DisplayMethod CurrentDisplayMethod { get; private set; }
        /// <summary>
        /// Pole dające dostęp do funkcji OpenGl'a
        /// </summary>
        private GlInterface _gl;
        /// <summary>
        /// Pole przechowujące dane wierzchołków
        /// </summary>
        private VerticesArray _vertices;
        /// <summary>
        /// Pole przechowujące program shaderów
        /// </summary>
        private ShaderProgram? _currentShader;
        /// <summary>
        /// Pole pamiętająće czy nastąpiła zmiana parametrów modelu i czy wymagane jest przeładowanie shaderów
        /// </summary>
        private bool _shaderChanged = true;
        /// <summary>
        /// Pole przechowujące załadowane assety
        /// </summary>
        private Assets _assets;
        /// <summary>
        /// Pole reprezentujące ścieżkę do aktualnej tekstury
        /// </summary>
        private string? _currentTexture;
        /// <summary>
        /// Pole przechowujące aktualnie załadowany kolor
        /// </summary>
        private byte[] _color = new byte[] { 244, 244, 244};
        /// <summary>
        /// Pole przechowujące pozycję światła na X
        /// </summary>
        private float _lightX = 6f;
        /// <summary>
        /// Pole przechowujące pozycję światła na Y
        /// </summary>
        private float _lightY = -8f;
        /// <summary>
        /// Pole przechowujące pozycję światła na Z
        /// </summary>
        private float _lightZ = 10f;
        /// <summary>
        /// Właściwość prezentująca pozycję światła na osi X
        /// </summary>
        public float LightX
        {
            get => _lightX;
            set
            {
                if (value != _lightX)
                {
                    _lightX = value;
                    _shaderChanged = true;
                }
                    
            }
        }
        /// <summary>
        /// Włąściwość prezentująca pozycję światła na osi Y
        /// </summary>
        public float LightY
        {
            get => _lightY;
            set
            {
                if (value != _lightY)
                {
                    _lightY = value;
                    _shaderChanged = true;
                }
                    
            }
        }
        /// <summary>
        /// Właściwość prezentująca pozycję światła na osi Z
        /// </summary>
        public float LightZ
        {
            get => _lightZ;
            set
            {
                if (value != _lightZ)
                {
                    _lightZ = value;
                    _shaderChanged = true;
                }
                    
            }
        }
        /// <summary>
        /// Pole określające aktualnie używaną technikę oświetlenia
        /// </summary>
        private string _lightTechnique = "phong_light";

        /// <summary>
        /// Konstruktor domyślny
        /// </summary>
        public ModelObject()
        {
            _assets = LoadShaderAssets();
            IsLoaded = false;
            UsesLight = false;
            UsesTexture = false;
            CurrentDisplayMethod = DisplayMethod.Standard;
            _vertices = new VerticesArray(null);
        }
        
        /// <summary>
        /// Laduje zestaw assetow w postaci listy plikow shaderow do latwiejszego ladowania
        /// </summary>
        /// <returns>Obiekt typu Assets, który reprezentuje rózne modele</returns>
        private Assets LoadShaderAssets()
        {
            var assetsPath = "./assets/shaders/";
            var assets = new Assets();

            assets.NewAsset(new Asset(0, "phong_light_v", assetsPath + "phong.vert"));
            assets.NewAsset(new Asset(1, "phong_light_f", assetsPath + "phong.frag"));

            assets.NewAsset(new Asset(2, "no_light_text_v", assetsPath + "vert_shader.vert"));
            assets.NewAsset(new Asset(3, "no_light_text_f", assetsPath + "frag_shader.frag"));
            
            assets.NewAsset(new Asset(4, "gooch_v", assetsPath + "gooch.vert"));
            assets.NewAsset(new Asset(5, "gooch_f", assetsPath + "gooch.frag"));
            
            assets.NewAsset(new Asset(6, "toon_v", assetsPath + "toon.vert"));
            assets.NewAsset(new Asset(7, "toon_f", assetsPath + "toon.frag"));

            return assets;
        }

        /// <summary>
        /// Ładuje dane z pliku obj o podanej sciezce
        /// </summary>
        /// <param name="filePath">Sciezka pliku obj</param>
        /// <exception cref="FileNotFoundException">Gdy nie znaleziono pliku</exception>
        /// <exception cref="FileIsNotProperObjFileException">Gdy plik nie jest w poprawnym formacie</exception>
        /// 
        public void LoadModelWithFile(string filePath)
        {
            if (!File.Exists(filePath))
                throw new FileNotFoundException($"Obj file: {filePath} does not exist!");

            var objLoaderFactory = new ObjLoaderFactory();
            var objLoader = objLoaderFactory.Create();
            var objFileStream = new FileStream(filePath, FileMode.Open);
            
            LoadResult? objFileResult = null;
            try
            {
                objFileResult = objLoader.Load(objFileStream);
            }
            catch (Exception e)
            {
                throw new FileIsNotProperObjFileException("Could not load obj model from this file");
            }
            
            if (objFileResult is null)
                throw new FileIsNotProperObjFileException("Could not load obj model from this file");

            var Vertices = new List<Vertex>();
            var Textures = new List<Texture>();
            var Normals = new List<Normal>();
            var Groups = new List<Group>();
            var Materials = new List<Material>();
            
            Vertices.AddRange(objFileResult.Vertices);
            Textures.AddRange(objFileResult.Textures);
            Normals.AddRange(objFileResult.Normals);
            Groups.AddRange(objFileResult.Groups);
            Materials.AddRange(objFileResult.Materials);

            _vertices = new VerticesArray(new List<VertexMRE>(VertexDataMapper.MapVertices(Vertices, Normals, Textures)));
            
            IsLoaded = true;
            
            verticesGl = _vertices.ToOpenGlArray();
            if (Groups.Count == 0)
            {
                indices = new uint[_vertices.Vertices.Count];
                for (uint i = 0; i < indices.Length; i++) indices[i] = i;
            }
            else
            {
                indices = VertexDataMapper.MapIndices(Groups[0].Faces);
            }
            if (!Normals.Any())
                VertexMapperMathHelper.CaculateVetexNormals(_vertices, indices);
            
            verticesGl = _vertices.ToOpenGlArray();
        }

        /// <summary>
        /// Inicjalizuje opengl'a w obiekcie (przekazywane z inita kontrolki openglowej)
        /// </summary>
        /// <param name="gl">obiekt klasy dający dostęp do funkcji opengl'a</param>
        public void InitializeDraw(GlInterface gl)
        {
            renderer = new Renderer(gl);

            _currentShader = null;

            _gl = gl;
            
        }
        /// <summary>
        /// Inicjalizuje i ładuje shadery do wyświetlania modelu
        /// </summary>
        /// <exception cref="ShaderProgramWasNotLoadedException">Kiedy nie udalo sie zaladowac shaderu</exception>
        public void InitShader()
        {
            if (!_shaderChanged)
                return;

            ShaderProgram? shaderProgram = null;
            
            if (!UsesLight)
            {
                shaderProgram = ShaderLoader.LoadShaderProgram(_gl,
                    _assets["no_light_text_v"].Value.Path,
                    _assets["no_light_text_f"].Value.Path
                );

                if (shaderProgram == null)
                {
                    throw new ShaderProgramWasNotLoadedException("Shader Program Not Loaded!");
                }

                Console.WriteLine("Shader Program Loaded.");
                
                shaderProgram.Bind();
            }
            else
            {
                shaderProgram = ShaderLoader.LoadShaderProgram(_gl,
                    _assets[$"{_lightTechnique}_v"].Value.Path,
                    _assets[$"{_lightTechnique}_f"].Value.Path
                );

                if (shaderProgram == null)
                {
                    throw new Exception("Shader Program Not Loaded!");
                }

                Console.WriteLine("Shader Program Loaded.");
            
            
                shaderProgram.Bind();
                //shaderProgram.SetUniform4f("u_Color", new Vector4(0.2f, 0.9f, 0.3f, 1.0f));
                shaderProgram.SetUniform3f("u_lightDir", new Vector3(LightX,  LightY, LightZ));
                
            }

            ModelRenderEngine.Texture texture;

            if (UsesTexture) 
            {
                texture = new(_gl, _currentTexture);
            }
            else
            {
                texture = new(_gl, _color[0], _color[1], _color[2]);
            }

            texture.Bind(0);
            shaderProgram.SetUniform1i("u_Texture", 0);

            shaderProgram.Unbind();
            _currentShader = shaderProgram;
            _shaderChanged = false;
        }

        /// <summary>
        /// Metoda, która renderuje przy użyciu OpenGl'a model 3d w jego widoku,
        /// przechowuje informację o translacji i obrocie kamery w oknie wyświetlania (obroty, translacja ta nie wpływa na sam model)
        /// </summary>
        /// <param name="x_translate">Translacja na osi X</param>
        /// <param name="y_translate">Translacja na osi Y</param>
        /// <param name="Z_translate">Translacja na osi Z</param>
        /// <param name="x_rotation">Obrót wokół osi X</param>
        /// <param name="y_rotation">Obrót wokół osi Y</param>
        /// <param name="_width">Szerokość okna wyświetlania w pixelach</param>
        /// <param name="_height">Wysokóśc okna wyświetlania w pixelach</param>
        public void Draw(float x_translate, float y_translate, float Z_translate, float x_rotation, float y_rotation, float _width, float _height)
        {
            
            var model = Matrix4.Identity;
            model *= Matrix4.CreateFromAxisAngle(new Vector3(0.0f, 1.0f, 0.0f), MathHelper.DegreesToRadians(y_rotation));
            model *= Matrix4.CreateFromAxisAngle(new Vector3(1.0f, 0.0f, 0.0f), MathHelper.DegreesToRadians(x_rotation));
            model *= Matrix4.CreateTranslation(new Vector3(x_translate, y_translate, Z_translate));
                    
            var proj = Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(70.0f), (float)(_width / _height), 0.1f, 200.0f);
            var view = Matrix4.CreateTranslation(0.0f, -0.5f, -10.0f);

            _currentShader.Bind();
            _currentShader.SetUniformMat4f("model", ref model);
            _currentShader.SetUniformMat4f("proj", ref proj);
            _currentShader.SetUniformMat4f("view", ref view);

            renderer = new Renderer(_gl);

            va = new VertexArray(_gl);
            vbo = new VertexBuffer(_gl, verticesGl);
            ibo = new IndexBuffer(_gl, indices);
                     
            VertexBufferLayout vbl = new();
            vbl.Push(GL_FLOAT, 3);
            if (_vertices.Vertices[0].Normal.HasValue)
                vbl.Push(GL_FLOAT, 3);
            if (_vertices.Vertices[0].TextureMapping.HasValue)
                vbl.Push(GL_FLOAT, 2);
                     
            va.AddBuffer(vbo, vbl);
            va.Unbind();
            vbo.Unbind();
            ibo.Unbind();

            Renderer.RenderingMode rMode = CurrentDisplayMethod switch
            {
                DisplayMethod.Standard => Renderer.RenderingMode.Normal,
                DisplayMethod.Wireframe => Renderer.RenderingMode.Wireframe
            };
            
            renderer.Clear();
            renderer.Draw(va, ibo, _currentShader, rMode);
            
        }

        /// <summary>
        /// Metoda, która skaluje model 3d o odpowiednią wartość skalowania
        /// </summary>
        /// <param name="scaleFactor">Od -1 do 1, gdzie 0 to domyślna wartość</param>
        public void Scale(float scaleFactor)
        {
            scaleFactor += 1;
            scaleFactor *= 0.5f * 1.5f;
            scaleFactor += 0.5f;
            int len = 3;
            if (_vertices.Vertices[0].Normal.HasValue) len += 3;
            if (_vertices.Vertices[0].TextureMapping.HasValue) len += 2;
            for (int i = 0; i < _vertices.Vertices.Count();i++) {
                verticesGl[i * len+1] +=1;
                verticesGl[i*len] *= scaleFactor;
                verticesGl[i*len+1] *= scaleFactor;
                verticesGl[i*len+2] *= scaleFactor;
            }
        }

        /// <summary>
        /// Metoda, która obraca model 3d o podaną wartość 
        /// </summary>
        /// <param name="rotateFactor">Wartość obrotu w radianach</param>
        public void Rotate(float rotateFactor)
        {
            rotateFactor = (float)rotateFactor * (float)Math.PI;
            int len = 3;
            if (_vertices.Vertices[0].Normal.HasValue) len += 3;
            if (_vertices.Vertices[0].TextureMapping.HasValue) len += 2;
            for (int i = 0; i < _vertices.Vertices.Count(); i++)
            {
                //x2=cosβx1−sinβy1y2=sinβx1+cosβy1
                var x = verticesGl[i*len];
                var z = verticesGl[i*len+2];

                verticesGl[i * len] = (float)Math.Cos(rotateFactor)*x- (float)Math.Sin(rotateFactor)*z;
                verticesGl[i * len + 2] = (float)Math.Cos(rotateFactor) * z + (float)Math.Sin(rotateFactor) * x;
            }
        }

        /// <summary>
        /// Metoda ustwiająca odpowiednia teksturę do wyświetlenia na modelu
        /// </summary>
        /// <param name="texturePath">Ścieżka do pliku tekstury</param>
        public void PutTexture(string texturePath)
        {
            _currentTexture = texturePath;
            UsesTexture = true;
            _shaderChanged = true;
        }
        /// <summary>
        /// Metoda, która wyłącza wyświetlania tekstury na modelu
        /// </summary>
        public void ResetTexture()
        {
            UsesTexture = false;
            _shaderChanged = true;
            _currentTexture = null;
        }
        /// <summary>
        /// Metoda, która ustawia odpowiedni kolor w formacie rgb dla wyświetlania modelu
        /// </summary>
        /// <param name="colors">Tablica byte'ów przechowująca informację o kolorze w formacie rgb</param>
        public void PutColor(byte[] colors) {
            UsesTexture = false;
            _color = (byte[]) colors.Clone();
            _shaderChanged = true;
        }
        
        /// <summary>
        /// Metoda ustawiająca odpowiedni tryb wyświetlania do renderowania modelu
        /// </summary>
        /// <param name="method">Enum z pliku DisplayMethod.cs</param>
        public void SetDisplayMethod(DisplayMethod method)
        {
            CurrentDisplayMethod = method;
        }

        /// <summary>
        /// Metoda, która włącza użycie świał przy użyciu modelu
        /// </summary>
        public void UseLight()
        {
            UsesLight = true;
            _shaderChanged = true;
        }
        /// <summary>
        /// Ustawia odpowiednia technike oswietlenia do renderowania modelu
        /// </summary>
        /// <param name="technique">nazwa techiki oświetlenia</param>
        public void UseLightTechnique(string technique)
        {
            _lightTechnique = technique switch
            {
                "Phong" => "phong_light",
                "Toon" => "toon",
                "Gooch" => "gooch"
            };
            _shaderChanged = true;
        }
        /// <summary>
        /// Metoda, która wyłącza używanie światła przy renderowaniu modelu
        /// </summary>
        public void StopUsingLight()
        {
            UsesLight = false;
            _shaderChanged = true;
        }
        
        /// <summary>
        /// Metoda, która używając klasy ObjFileWriter zapisuje aktualny stan modelu do pliku obj o podanej ścieżce
        /// </summary>
        /// <param name="path">Scieżka modelu</param>
        /// <exception cref="ModelObjectWasNotLoadedException">Gdy następuje próba wywołania metody przed załadowaniem modelu</exception>
        public void SaveToObjFile(string path)
        {
            if (!this.IsLoaded)
                throw new ModelObjectWasNotLoadedException("Model file was not loaded");
            
            var verts = VerticesArray.Of(verticesGl,
                _vertices.Vertices[0].Normal is Vector3,
                _vertices.Vertices[0].TextureMapping is Vector2);
            
            using (var fileWriter = new StreamWriter(File.OpenWrite(path)))
            {
                var objWriter = new ObjFileWriter(fileWriter);
                objWriter.WriteFile(verts, indices);
            }
        }
    }

    #region WyjątkiModelu

    /// <summary>
    /// Klasa wyjątku wyrzucanego gdy załadowany plik ma niepoprawny format
    /// </summary>
    public class FileIsNotProperObjFileException : Exception
    {
        /// <summary>
        /// Konstruktor z parametrem wiadomości
        /// </summary>
        /// <param name="message">Treść wiadomości do wyświetlenia w wyjątku</param>
        public FileIsNotProperObjFileException([CanBeNull] string? message)
            : base(message)
        {
        }
    }
    /// <summary>
    /// Klasa wyjątku wyrzucanego gdy model nie jest załadowany, a użytkownik próbuje dokonać jakiś czynności na nim
    /// Z poziomu interfejscu GUI nie jest możliwe wywołanie tego wyjątku, jedynie używając kodu bezpośrednio
    /// </summary>
    public class ModelObjectWasNotLoadedException : Exception
    {
        /// <summary>
        /// Konstruktor z parametrem wiadomośći
        /// </summary>
        /// <param name="message">Treść wiadomości do wyświetlenia w wyjątku</param>
        public ModelObjectWasNotLoadedException([CanBeNull] string? message)
            : base(message)
        {
        }
    }
    /// <summary>
    /// Klasa wyjątku wyrzucana w momencie gdy nie udało się załadować shaderów do modelu
    /// </summary>
    public class ShaderProgramWasNotLoadedException : Exception
    {
        /// <summary>
        /// Konstruktor z parametrem wiadomości
        /// </summary>
        /// <param name="message">Treść wiadomości do wyświetlenia w wyjątku</param>
        public ShaderProgramWasNotLoadedException([CanBeNull] string? message) 
            : base(message)
        {
        }
    }

    #endregion
}


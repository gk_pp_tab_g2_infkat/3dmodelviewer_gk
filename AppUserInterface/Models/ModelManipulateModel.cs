using ViewerLogicLayer;

namespace AppUserInterface.Models
{
    
    /// <summary>
    /// Model dla viewmodelu ModelManipulateViewModel, który przechowuje dane zwiazane z skalowaniem i rotacja modelu
    /// </summary>
    public class ModelManipulateModel
    {
        /// <summary>
        /// Pole przechowujące wartość skalowania w kontrolce z GUI
        /// </summary>
        public float ScaleValue { get; set; } = 0.0f;
        /// <summary>
        /// Pole przechowująca wartość rotacji w kontrolce z GUI
        /// </summary>
        public float RotateValue { get; set; } = 0.0f;
        /// <summary>
        /// Pole informujące o użyciu światła lub nie z odpowiedniego radio button'a
        /// </summary>
        public bool UseLight { get; set; } = false;
        /// <summary>
        /// Pole informujące o wybranym trybie wyświetlania z GUI
        /// </summary>
        public DisplayMethod DisplayMode { get; set; } = DisplayMethod.Standard; 
        /// <summary>
        /// Pole przechowujące pozycję światła na osi X
        /// </summary>
        public float LightXPos { get; set; } = 0.0f;
        /// <summary>
        /// Pole przechowujące pozycję światła na osi Y
        /// </summary>
        public float LightYPos { get; set; } = 0.0f;
        /// <summary>
        /// Pole przechowujące pozycję światła na osi Z
        /// </summary>
        public float LightZPos { get; set; } = 0.0f;
    }
}

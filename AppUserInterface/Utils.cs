using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace AppUserInterface
{
    /// <summary>
    /// Klasa przechowująca użyteczne metody związane z systemem na którym aplikacja jest używana
    /// </summary>
    public class PlatformUtils
    {
        /// <summary>
        /// Metoda zwracająca informację czy uruchamiamy aplikację na systemie macOS
        /// </summary>
        /// <returns>True jeśli nasz system to macOS, False jeśli nie</returns>
        public static bool IsMacOS() => RuntimeInformation.IsOSPlatform(OSPlatform.OSX);
        /// <summary>
        /// Metoda zwracająca informację czy uruchamiamy aplikację na systemie Windows
        /// </summary>
        /// <returns>True jeśli nasz system to Windows, False jeśli nie</returns>
        public static bool IsWindows() => RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
        /// <summary>
        /// Metoda zwracająca informację czy uruchamiamy aplikację na systemie Linux
        /// </summary>
        /// <returns>True jeśli nasz system to Linux, Flase jeśli nie</returns>
        public static bool IsLinux() => RuntimeInformation.IsOSPlatform(OSPlatform.Linux);
        
        /// <summary>
        /// (Dotyczy tylko systemu macOS)
        /// Metoda zwracająca informację czy wyświetlacz, którego używamy to wyświetlacz Retina.
        /// </summary>
        /// <returns>True jeśli posiadamy ekran Retina, False jeśli nie</returns>
        /// <exception cref="PlatformIncorrectForThisMethodException">Jeśli wywołujemy metodę na systemie innym niż macOS</exception>
        public static bool IsRetinaDisplay()
        {
            if (!IsMacOS())
                throw new PlatformIncorrectForThisMethodException("Your OS is not macOS!");
            
            var displayString = RunShellCommand("system_profiler SPDisplaysDataType | grep \"Display Type\"");
            return displayString.Contains("Retina");
        }
        /// <summary>
        /// Metoda do wywoływania komend w systemowym shellu
        /// </summary>
        /// <param name="command">tekst komendy do uruchomienia</param>
        /// <returns>Tekst zwracany z komendy</returns>
        public static string RunShellCommand(string command)
        {
            if (IsWindows())
                return RunWindowsShellCommand(command);
            else
                return RunUnixShellCommand(command);
        }
        /// <summary>
        /// Metoda do wywoływania komend do shella na systemie Windows (Nie zaimplementowana, nie była potrzebna)
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        private static string RunWindowsShellCommand(string cmd)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Metoda do wywoływania komend shellowych na systemach unixowych (Linux i macOS)
        /// </summary>
        /// <param name="cmd">Treść komendy do użycia</param>
        /// <returns>Zwracany tekst z komendy</returns>
        private static string RunUnixShellCommand(string cmd)
        {
            var process = new ProcessStartInfo()
            {
              FileName  = "/bin/sh",
              Arguments = $"-c \"{cmd}\"",
              RedirectStandardOutput = true,
              UseShellExecute = false,
              CreateNoWindow = true
            };
            using var command = Process.Start(process);
            command!.WaitForExit();
            return command.StandardOutput.ReadToEnd();

        }
        
    }
    /// <summary>
    /// Klasa wyjątku wywoływanego w momencie gdy używana jest metoda nie przeznaczona na system, z którego jest uruchamiana
    /// </summary>
    public class PlatformIncorrectForThisMethodException : Exception
    {
        /// <summary>
        /// Konstruktor z parametrem wiadomości
        /// </summary>
        /// <param name="message">Wiadomość, którą ma wywoływać wyjątek</param>
        public PlatformIncorrectForThisMethodException(string message)
            : base(message)
        {
        }
    }
}

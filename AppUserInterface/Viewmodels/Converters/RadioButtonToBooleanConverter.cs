using System;
using System.Globalization;
using Avalonia.Data.Converters;

namespace AppUserInterface.Viewmodels.Converters
{
    /// <summary>
    /// Klasa, konwertująca wartości radio buttonów na boole
    /// </summary>
    public class RadioButtonToBooleanConverter : IValueConverter
    {
        public object? Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
        {
            if (value is bool val)
                return !(val);
            return value;
        }

        public object? ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
        {
            if (value is bool val)
                return !val;
            return value;
        }
    }
}

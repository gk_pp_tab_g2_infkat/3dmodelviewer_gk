using System;
using System.ComponentModel;
using System.Windows.Input;
using AppUserInterface.Models;
using ViewerLogicLayer;

namespace AppUserInterface.Viewmodels
{
    /// <summary>
    /// View model dla widoku ModelManipulateView, który przechowuje dane dla slidera rotacji i skalowania
    /// </summary>
    public class ModelManipulateViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Model encji danych manipulatora parametrami obiektu modelu 3d
        /// </summary>
        private ModelManipulateModel _model;
        /// <summary>
        /// Właściwość dająca dostęp do wartości skalowania modelu
        /// </summary>
        public float ScaleValue
        {
            get => _model.ScaleValue;
            set
            {
                if (_model.ScaleValue != value)
                {
                    _model.ScaleValue = value;
                    OnPropertyChanged(nameof(ScaleValue));
                }
            }
        }
        /// <summary>
        /// Właściwość dająca dostęp do wartośći rotacji modelu
        /// </summary>
        public float RotateValue
        {
            get => _model.RotateValue;
            set
            {
                if (_model.RotateValue != value)
                {
                    _model.RotateValue = value;
                    OnPropertyChanged(nameof(RotateValue));
                }
            }
        }
        /// <summary>
        /// Właściwość dająca dostęp do aktualnej metody wyświetlania modelu
        /// </summary>
        public DisplayMethod CheckedDisplayMode
        {
            get => _model.DisplayMode;
            set
            {
                if (_model.DisplayMode != value)
                {
                    Console.WriteLine(value);
                    _model.DisplayMode = value;
                    OnPropertyChanged(nameof(CheckedDisplayMode));
                }
            }
        }
        /// <summary>
        /// Właściwość dająca dostęp do zminnej informujący o użyciu światła 
        /// </summary>
        public bool UseLight
        {
            get => _model.UseLight;
            set
            {
                if (_model.UseLight != value)
                {
                    _model.UseLight = value;
                    OnPropertyChanged(nameof(UseLight));
                }
            }
        
        }
        /// <summary>
        /// Właściwość dająca dostęp do zmiennej pozycji światła na osi X
        /// </summary>
        public float LightXPos
        {
            get => _model.LightXPos;
            set
            {
                if (_model.LightXPos != value)
                {
                    _model.LightXPos = value;
                    OnPropertyChanged(nameof(LightXPos));
                }
            }
        }
        /// <summary>
        /// Właściwość dająca dostęp do zmiennej pozycji światła na osi Y
        /// </summary>
        public float LightYPos
        {
            get => _model.LightYPos;
            set
            {
                if (_model.LightYPos != value)
                {
                    _model.LightYPos = value;
                    OnPropertyChanged(nameof(LightYPos));
                }
            }
        }
        /// <summary>
        /// Właściwość dająca dostęp do zmiennej pozycji światła na osi Z
        /// </summary>
        public float LightZPos
        {
            get => _model.LightZPos;
            set
            {
                if (_model.LightZPos != value)
                {
                    _model.LightZPos = value;
                    OnPropertyChanged(nameof(LightZPos));
                }
            }
        }
        
        /// <summary>
        /// Konstruktor domyślny
        /// </summary>
        public ModelManipulateViewModel()
        {
            _model = new ModelManipulateModel();
            
        }
        /// <summary>
        /// Zdarzenie wywoływane przy zmianie wartości dowolnej z włąściwości
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged is not null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Loader;
using System.Threading.Tasks;
using AppUserInterface.Views;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Dialogs;
using Avalonia.Input;
using Avalonia.Interactivity;
using ModelRenderEngine;
using ViewerLogicLayer;
using Pointer = Avalonia.Input.Pointer;

namespace AppUserInterface
{
    /// <summary>
    /// Klasa głównego widoku okna aplikacji
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Obiekt myszki
        /// </summary>
        private readonly Pointer _mouse;
        /// <summary>
        /// Zestaw assetow - pliki shaderow
        /// </summary>
        private readonly Assets _assets;
        /// <summary>
        /// Aktualna pozycja myszki
        /// </summary>
        private Point _curPosition;
        /// <summary>
        /// Czy wcisnieto i trzyma sie przycisk myszki
        /// </summary>
        private bool _startedDraging = false;
        /// <summary>
        /// Czy wcinieto shift
        /// </summary>
        private bool _startedDragingShift = false;
        /// <summary>
        /// Czy wcisnieto ctrl
        /// </summary>
        private bool _startedDragingCtrl = false;

        /// <summary>
        /// Obiekt reprezentujący model 3d
        /// </summary>
        private ModelObject _model3D = new ModelObject();
        
        /// <summary>
        /// Getter dla modelu3d
        /// </summary>
        public ModelObject Model3d
        {
            get => _model3D;
        }
        
        /// <summary>
        /// Konstruktor domyślny dla głównego okna applikacji
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            ManipulateView.ParentWindow = this;
            
            // Nowy pointer
            _mouse = new Pointer(0, PointerType.Mouse, true);
            // Lapie dane tylko gdy jest w kontrolce OpenGL'owej (czarne tlo z modelem)
            _mouse.Capture(this.ModelView); 

            // Dodanie handlera do eventu template argumentow metody: (PointerCostamEvent, (sender, args) => {})
            // mozna tez podpiac normalna metode w klasie (PointerCostamEvent), HandlerMetoda)
            _mouse.Captured.AddHandler(PointerPressedEvent, ((sender, args) =>
            {
                _curPosition = args.GetPosition(this.ModelView); // Pobranie aktualnie pozycji
                //Console.WriteLine($"[Curr Mouse: {curPosition.X} x {curPosition.Y}]");
                if (args.KeyModifiers == KeyModifiers.None)
                {
                    _startedDraging = true;

                }
                else if (args.KeyModifiers == KeyModifiers.Control)
                {
                    _startedDragingCtrl = true;
                }
                else if (args.KeyModifiers == KeyModifiers.Shift)
                {
                    _startedDragingShift = true;
                }
                
                
            }));
            
            _mouse.Captured.AddHandler(PointerMovedEvent, ((sender, args) =>
            {
                var nextPos = args.GetPosition(this.ModelView);
                
                if (_startedDraging) 
                {
                    this.ModelView.y_rotation += (float) ((nextPos.X - _curPosition.X) / 5);
                    this.ModelView.x_rotation += (float) ((nextPos.Y - _curPosition.Y) / 5);
                    Console.WriteLine($"[Rotated by: {nextPos.X - _curPosition.X} {nextPos.Y - _curPosition.Y}]");
                } else if (_startedDragingCtrl)
                {
                    //this.ModelView.Z_translate += (float)((nextPos.X - _curPosition.X) / 10);
                    this.ModelView.x_translate += (float) ((nextPos.X - _curPosition.X) / 80);
                    this.ModelView.y_translate += (float) ((nextPos.Y - _curPosition.Y) / 80);
                    Console.WriteLine($"[Moved by: {nextPos.X - _curPosition.X} {nextPos.Y - _curPosition.Y}]");
                } else if (_startedDragingShift)
                {
                    this.ModelView.Z_translate += (float) ((nextPos.Y - _curPosition.Y) / 40);
                    Console.WriteLine($"[Moved on Z by: [{nextPos.Y - _curPosition.Y}]");
                }

                
                _curPosition = nextPos;
            }));
            
            _mouse.Captured.AddHandler(PointerReleasedEvent, (sender, args) =>
            {
                if (_startedDraging)
                {
                    var nextPos = args.GetPosition(this.ModelView);
                    Console.WriteLine($"[Moved by: {nextPos.X - _curPosition.X} {nextPos.Y - _curPosition.Y}]");
                    _curPosition = nextPos;
                    _startedDraging = false;
                }
                else if (_startedDragingCtrl)
                { 
                    _startedDragingCtrl = false;
                }
                else if (_startedDragingShift)
                    _startedDragingShift = false;

            });
            
            this.LogicalChildren.Add(this.ModelView);
            this.LogicalChildren.Add(this.ManipulateView);

            Console.WriteLine($"{this.Width}x{this.Height}");

            // Działanie gdy wywoływane sa eventy z klasy ModelManipulateView
            ManipulateView.ScaleFactorChanged += OnScaleFactorChanged;
            ManipulateView.RotateFactorChanged += OnRotateFactorChanged;
            ManipulateView.DisplayModeChanged += OnDisplayModeChanged;
            ManipulateView.TextureChange += OnTextureChange;
            ManipulateView.TextureReset += OnTextureReset;
            ManipulateView.LightModeChanged += OnLightChange;
            ManipulateView.SaveModelClicked += OnSaveModelObject;
            ManipulateView.LigthXPosChanged += OnLightPosXChanged;
            ManipulateView.LigthYPosChanged += OnLightPosYChanged;
            ManipulateView.LigthZPosChanged += OnLightPosZChanged;
            ManipulateView.RGBChanged += OnRgbChanged;
            ManipulateView.LightTechniqueChanged += OnLightTechniqueChanged;
        }

        /// <summary>
        /// Metoda do wybierania pliku z syststemu
        /// </summary>
        /// <returns>Opcjonalny string będący scieżką do wybranego pliku</returns>
        private async Task<string?> GetPickedFilePath()
        {
            var openFDialog = new OpenFileDialog();
            openFDialog.Filters.Add(new FileDialogFilter() { Name = "OBJ Model File", Extensions = {"obj"}});//pierwsze powinno być to elo
            openFDialog.Filters.Add(new FileDialogFilter() { Name = "Text", Extensions = { "txt" } });
            
            openFDialog.AllowMultiple = false;

            var path = await openFDialog.ShowAsync(this);
            if (path is null || path.Length == 0)
                return null;
            return path[0];
        }

        /// <summary>
        /// Metoda wywoływana po wciśnięciu opcji Open z menu File
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void MenuItem_OnClick(object? sender, RoutedEventArgs e)
        {
            var path = await GetPickedFilePath();
            Console.WriteLine(path ?? "None");

            if (path is null) return;
            
            try
            {
                _model3D.LoadModelWithFile(path);
                ModelView.x_rotation = 0f;
                ModelView.y_rotation = 0f;
                ModelView.x_translate = 0f;
                ModelView.y_translate = 0f;
                ModelView.Z_translate = 0f;
                
                this.Title = "Model [" + path + "]";
                ManipulateView.MakeModelControlsVisible();
            }
            catch (Exception ex)
            {
                ErrorMessageDialog.Show(this,  "Could not load file " + path + ".\nMay be in wrong format.", "File error");
            }
            
            // _model3D.LoadModelWithFile(path);
            //ManipulateView.MakeModelControlsVisible();
        }
        
        /// <summary>
        /// Metoda wywoływana gdy wciśniemy przycisk od skali w okienku
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="scaleFactor"></param>
        private void OnScaleFactorChanged(object? sender, float scaleFactor)
        {
            int _scale = 0;
            Console.WriteLine($"Scale factor: {scaleFactor}");
            _model3D.Scale(scaleFactor);
        }

        /// <summary>
        /// Metoda wywoływana gdy wciśniemy przycisk od rotacji w okienku
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="rotateFactor"></param>
        private void OnRotateFactorChanged(object? sender, float rotateFactor)
        {
            Console.WriteLine($"Rotate factor: {rotateFactor}");
            _model3D.Rotate(rotateFactor);
        }

        /// <summary>
        /// Metoda wywoływana gdy zmieni sie tryb wyswietlania
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="method"></param>
        private void OnDisplayModeChanged(object? sender, DisplayMethod method)
        {
            Console.WriteLine("Dislay method: " + method);
            _model3D.SetDisplayMethod(method);
        }

        /// <summary>
        /// Metoda wywoływana gdy wcisniemy przycisk put texture i wybierzemy odpowiedni plik
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="useTexture"></param>
        private void OnTextureChange(object? sender, string useTexture)
        {
            Console.WriteLine($"On texture: {useTexture}");
            _model3D.PutTexture(useTexture);
        }
        
        private void OnTextureReset(object? sender, bool resetTexture)
        {
            _model3D.ResetTexture();
        }

        /// <summary>
        /// Metoda wywoływana gdy wciśniemy przycisk związany z użyciem światła w okienku
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="light"></param>
        private void OnLightChange(object? sender, bool light)
        {
            Console.WriteLine($"On light: {light}");
            if (light)
                _model3D.UseLight();
            else 
                _model3D.StopUsingLight();
        }
        
        /// <summary>
        /// Metoda wywoływana gdy wcisniemy przycisk do zapisu modelu do nowego pliku 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async Task OnSaveModelObject(object? sender, EventArgs e)
        {
            var saveFDialog = new SaveFileDialog();
            saveFDialog.DefaultExtension = "obj";
            saveFDialog.Filters.Add(new FileDialogFilter() {Name = "obj", Extensions = {"obj"}});

            var fpath = await saveFDialog.ShowAsync(this);
            if (fpath is String path)
            {
                try
                {
                    _model3D.SaveToObjFile(path);
                    ErrorMessageDialog.Show(this, "File was saved successfuly", "File Saved");
                }
                catch (Exception ex)
                {
                    ErrorMessageDialog.Show(this, "File was NOT saved successfuly", "File Error");
                }
                
            }
        }
        /// <summary>
        /// Metoda wywoływana gdy zmienimy pozycje swiatla na osi X
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="value"></param>
        private void OnLightPosXChanged(object? sender, float value)
        {
            Console.WriteLine(value);
            _model3D.LightX += value;
        }
        /// <summary>
        /// Metoda wywolywana gdy zmienimy pozycje swiatla na osi Y
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="value"></param>
        private void OnLightPosYChanged(object? sender, float value)
        {
            _model3D.LightY += value;
        }
        /// <summary>
        /// Metoda wywlywana gdy zmienimy pozycje swiatla na osi Z
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="value"></param>
        private void OnLightPosZChanged(object? sender, float value)
        {
            _model3D.LightZ += value;
        }
        /// <summary>
        /// Metoda wywolywana przy zmiane w comboboxie techniki renderowania swiatla 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="technique"></param>
        private void OnLightTechniqueChanged(object? sender, string technique)
        {
            _model3D.UseLightTechnique(technique);
        }
        /// <summary>
        /// Metoda wywolywana przy wcisnieciu przyciku zwiazanego z nalozeniem koloru na model (resetuje nalozona teksture)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="value"></param>
        private void OnRgbChanged(object? sender, byte[] value) 
        {
            _model3D.PutColor(value);
        }

        private void Help_OnClick(object? sender, RoutedEventArgs e)
        {
            var helpMessage = "  Model ładuje się z menu kontekstowego Open.  \n\n" +
                              "  Aby obracać model wciśnić LPM i przesuwaj kursorem po czarnym ekranie,  \n" +
                              "  Aby przybliżyć model wciśnij LPM wraz Shift i przesuwaj kursorem do góry  \n" +
                              "  aby przyblizyć w dół aby oddalić  \n" +  
                              "  Aby przesunąć model wciskamy LPM + CTRL i przesuwamy kursorem  \n" +
                              "  Lewy pasek służy do interakcji z modelem, zmiany koloru, oświetlenia, tekstur  \n" +
                              "  pozycji światła a także zapisania modelu do nowego pliku.  \n" +
                              "  Model można też skalować i rotować co edytuje wierzchołki i pozwala na nadpisanie pliku obj.  \n" +
                              "  Model koloruje się wpisując wartości od 0 - 255 w przestrzeni RGB, wpisanie błędnej wartości  \n" +
                              "  nie spowoduje zmiany koloru, po nałozeniu tekstury i jej resecie model wraca do ostatniego wybranego koloru  \n" +
                              "  stan ustawień jest zapamiętywany między ładowanymi modelami co przyspiesza pracę  \n";
            
            ErrorMessageDialog.Show(this, helpMessage ,"Help");
        }

        private void Exit_OnClick(object? sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Threading.Tasks;
using AppUserInterface.Viewmodels;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Microsoft.CodeAnalysis.Emit;
using ViewerLogicLayer;

namespace AppUserInterface
{
    /// <summary>
    /// Klasa reprezentująca pole w oknie programu związane z edycją modelu 3d
    /// !! (skonczone tu nic nie zmieniac) !!
    /// </summary>
    public partial class ModelManipulateView : UserControl
    {
        private TextBlock _modelNotLoaded;
        private StackPanel _controlPanel;
        private Slider _scaleSlider;
        private Slider _rotateSlider;
        private RadioButton _wireframeRadio;
        private RadioButton _standardRadio;
        private RadioButton _noLightRadio;
        private RadioButton _useLightRadio;
        private TextBox _R;
        private TextBox _G;
        private TextBox _B;
        private ComboBox _lightTechnique;

        private readonly ModelManipulateViewModel _viewModel;
        
        public Window ParentWindow { get; set; }
        
        public ModelManipulateView()
        {
            InitializeComponent();
            
            _modelNotLoaded = this.FindControl<TextBlock>("ModelNotLoadedTxt");
            _controlPanel = this.FindControl<StackPanel>("ControlPanel");
            _scaleSlider = this.FindControl<Slider>("ScaleSlider");
            _rotateSlider = this.FindControl<Slider>("RotateSlider");
            _wireframeRadio = this.FindControl<RadioButton>("WireframeRadio");
            _standardRadio = this.FindControl<RadioButton>("StandardRadio");
            _noLightRadio = this.FindControl<RadioButton>("NoLightRadio");
            _useLightRadio = this.FindControl<RadioButton>("LightRadio");
            _R = this.FindControl<TextBox>("R");
            _G = this.FindControl<TextBox>("G");
            _B = this.FindControl<TextBox>("B");
            _lightTechnique = this.FindControl<ComboBox>("LightTechniqueCB");
            
            _viewModel = new ModelManipulateViewModel();
            _viewModel.PropertyChanged += OnViewModelPropertyChanged;
            this.DataContext = _viewModel;
        }

        public void MakeModelControlsVisible()
        {
            _modelNotLoaded.IsVisible = false;
            _controlPanel.IsVisible = true;
        }

        private void OnViewModelPropertyChanged(object? sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(_viewModel.UseLight):
                    OnLightModeChanged(_viewModel.UseLight);
                    break;
                case nameof(_viewModel.CheckedDisplayMode):
                    OnDisplayModeChanged(_viewModel.CheckedDisplayMode);
                    break;
                case nameof(_viewModel.LightXPos):
                    OnLightXPosChanged(_viewModel.LightXPos);
                    break;
                case nameof(_viewModel.LightYPos):
                    OnLightYPosChanged(_viewModel.LightYPos);
                    break;
                case nameof(_viewModel.LightZPos):
                    OnLightZPosChanged(_viewModel.LightZPos);
                    break;
            }
        }
        
        // Eventy zeby wysylac dane do glownego okna bo cos nie dziala w parent window property idk czemu chyba cos zle robie
        
        public event EventHandler<float> ScaleFactorChanged;
        protected virtual void OnScaleFactorChanged(float scaleFactor)
        {
            ScaleFactorChanged?.Invoke(this, scaleFactor);
        }
        
        public event EventHandler<float> RotateFactorChanged;
        protected virtual void OnRotateFactorChanged(float rotateFactor)
        {
            RotateFactorChanged?.Invoke(this, rotateFactor);
        }
        
        public event EventHandler<DisplayMethod> DisplayModeChanged;
        protected virtual void OnDisplayModeChanged(DisplayMethod method)
        {
            DisplayModeChanged?.Invoke(this, method);
        }
        
        public event EventHandler<string> TextureChange;
        protected virtual void OnTextureChange(string texturePath)
        {
            TextureChange?.Invoke(this, texturePath);
        }

        public event EventHandler<bool> TextureReset;
        protected virtual void OnTextureReset(bool reset)
        {
            TextureReset?.Invoke(this, reset);
        }
        
        public event EventHandler<bool> LightModeChanged;
        protected virtual void OnLightModeChanged(bool light)
        {
            LightModeChanged?.Invoke(this, light);
        }
        
        public delegate Task AsyncEventHandler(object sender, EventArgs e);
        public event AsyncEventHandler SaveModelClicked;
        protected async virtual Task OnSaveModelClicked()
        {
            await SaveModelClicked?.Invoke(this, null);
        }

        public event EventHandler<float> LigthXPosChanged;
        protected virtual void OnLightXPosChanged(float change)
        {
            LigthXPosChanged?.Invoke(this, change);
        }
        public event EventHandler<float> LigthYPosChanged;
        protected virtual void OnLightYPosChanged(float change)
        {
            LigthYPosChanged?.Invoke(this, change);
        }
        public event EventHandler<float> LigthZPosChanged;
        protected virtual void OnLightZPosChanged(float change)
        {
            LigthZPosChanged?.Invoke(this, change);
        }

        public event EventHandler<byte[]> RGBChanged;
        protected virtual void OnRGBChanged(byte[] change) 
        {
            RGBChanged?.Invoke(this, change);
        }

        public event EventHandler<string> LightTechniqueChanged;
        protected virtual void OnLightTechniqueChanged(string newTechnique)
        {
            LightTechniqueChanged?.Invoke(this, newTechnique);
        }

        private void ScaleButton_OnClick(object? sender, RoutedEventArgs e)
        {
            OnScaleFactorChanged((float) _viewModel.ScaleValue);
        }

        private void RotateButton_OnClick(object? sender, RoutedEventArgs e)
        {
            OnRotateFactorChanged((float) _viewModel.RotateValue);
        }
        

        private async void LoadTextureButton_OnClick(object? sender, RoutedEventArgs e)
        {
            var openFDialog = new OpenFileDialog();
            openFDialog.Filters.Add(new FileDialogFilter() { Name = "png", Extensions = {"png"}}); 
            openFDialog.Filters.Add(new FileDialogFilter() { Name = "jpg", Extensions = {"jpg"}});
            openFDialog.Filters.Add(new FileDialogFilter() { Name = "jpeg", Extensions = {"jpeg"}});
            openFDialog.AllowMultiple = false;

            string[]? texturePath = await openFDialog.ShowAsync(ParentWindow);
            
            if (texturePath.Length == 1)
                OnTextureChange(texturePath[0]);
        }

        private void ResetButton_OnClick(object? sender, RoutedEventArgs e)
        {
            OnTextureReset(true);
        }

        private void SaveFile_OnClick(object? sender, RoutedEventArgs e)
        {
            OnSaveModelClicked();
        }

        private void PutColor_OnClick(object? sender, RoutedEventArgs e) 
        {
            try 
            {
                var R = Byte.Parse(_R.Text);
                var G = Byte.Parse(_G.Text);
                var B = Byte.Parse(_B.Text);
                OnRGBChanged(new [] {R,G,B});
            }
            catch (Exception ex) 
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void SelectingItemsControl_OnSelectionChanged(object? sender, SelectionChangedEventArgs e)
        {
            OnLightTechniqueChanged(((sender as ComboBox)?.SelectedItem as Label).Content.ToString());
        }
    }
}

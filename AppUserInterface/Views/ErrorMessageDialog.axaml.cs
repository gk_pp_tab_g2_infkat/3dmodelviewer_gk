using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace AppUserInterface.Views
{
    /// <summary>
    /// Klasa okienka z bledem
    /// </summary>
    public partial class ErrorMessageDialog : Window
    {
        /// <summary>
        /// Konstruktor domyslny
        /// </summary>
        public ErrorMessageDialog()
        {
            InitializeComponent();
    #if DEBUG
            this.AttachDevTools();
    #endif
        }
        /// <summary>
        /// Inicjalizacja komponentu i jego pliku AXAML
        /// </summary>
        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
        /// <summary>
        /// Funkcja pokazujaca nowe okna widoku
        /// </summary>
        /// <param name="parent">Okno z ktorego ma sie wyswietlic blad</param>
        /// <param name="text">tresc bledu</param>
        /// <param name="title">tytul bledu</param>
        public static void Show(Window? parent, string text, string title)
        {
            var errorMsgDial = new ErrorMessageDialog()
            {
                Title = title
            };
            errorMsgDial.FindControl<TextBlock>("ErrorText").Text = text;
            var buttonPanel = errorMsgDial.FindControl<StackPanel>("Buttons");
    
            var ok = new Button() {Content = "OK"};
            ok.Click += (_, __) =>
            {
                errorMsgDial.Close();
            };
            buttonPanel.Children.Add(ok);
    
            if (parent != null)
                errorMsgDial.ShowDialog(parent);
        }
    }
}


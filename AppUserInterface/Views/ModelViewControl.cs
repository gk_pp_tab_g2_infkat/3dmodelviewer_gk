using System;
using System.Diagnostics;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.OpenGL;
using Avalonia.OpenGL.Controls;
using Avalonia.Threading;
using static Avalonia.OpenGL.GlConsts;

using ModelRenderEngine;
using OpenTK.Mathematics;
using ViewerLogicLayer;

namespace AppUserInterface
{
    /// <summary>
    /// Klasa controlera, która wywołuje odpowiednie metody dla obiektu modelu 3d
    /// </summary>
    public class ModelViewControl : OpenGlControlBase
    {
        /// <summary>
        /// Obiekt reprezentujacy nasz model 3d
        /// </summary>
        private ModelObject _model;
        /// <summary>
        /// Wysokość viewportu
        /// </summary>
        private float _height = 0.0f;
        /// <summary>
        /// Szerokość viewportu
        /// </summary>
        private float _width = 0.0f;
        /// <summary>
        /// Rotacja modelu na osi X
        /// </summary>
        public float x_rotation { get; set; } = 0.0f;
        /// <summary>
        /// Rotacja modelu na osi Y
        /// </summary>
        public float y_rotation { get; set; } = 0.0f;
        /// <summary>
        /// Translacja modelu na osi X
        /// </summary>
        public float x_translate { get; set; } = 0.0f;
        /// <summary>
        /// Translacja modelu na osi Y
        /// </summary>
        public float y_translate { get; set; } = 0.0f;
        /// <summary>
        /// Translacja modelu na osi Z
        /// </summary>
        public float Z_translate { get; set; } = 0.0f;

        /// <summary>
        /// Konstruktor domyslny
        /// </summary>
        public ModelViewControl()
        {
            
        }
        
        /// <summary>
        /// Inicializacja OpenGl'a, viewportu oraz przypisanie do modelu obiektu OpenGL'a
        /// 
        /// </summary>
        /// <param name="gl">Obiekt z metodami do OpenGL</param>
        /// <param name="fb">?</param>
        /// <exception cref="Exception"></exception>
        protected override void OnOpenGlInit(GlInterface gl, int fb)
        {
            Console.WriteLine($"Renderer: {gl.GetString(GL_RENDERER)}\nVersion: {gl.GetString(GL_VERSION)}");

            gl.Enable(GL_BLEND);
            gl.BlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            
            // Ustawianie viewportu uwzględniające dziwne zachowania skalowania na macbookach z retiną
            var resScale = 1;
            if (PlatformUtils.IsMacOS() && PlatformUtils.IsRetinaDisplay())
                resScale = 2;

            _width = (float) (resScale * Bounds.Width);
            _height = (float) (resScale * Bounds.Height);

            gl.Viewport(0, 0, (int) _width, (int) _height);
            
            // Pobranie obiektu klasy glownego okna
            var mainWindow =
                (Application.Current.ApplicationLifetime as IClassicDesktopStyleApplicationLifetime)
                .MainWindow as MainWindow;
            
            // Przygotowanie modelu do dzialania z opengl
            mainWindow.Model3d.InitializeDraw(gl);  
            _model = mainWindow.Model3d;
        }

        /// <summary>
        /// Metoda bezpośrednio renderująca model w opengl'u w kontrolce opengl'a
        /// </summary>
        /// <param name="gl"></param>
        /// <param name="fb"></param>
        protected override void OnOpenGlRender(GlInterface gl, int fb)
        {

            gl.Clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            gl.Enable(GL_DEPTH_TEST);
            gl.ClearColor(0.0f, 0.0f, 0.0f, 1.0f);
            
            // Jesli plik modelu zaladowany rysuj moddel
            if (_model.IsLoaded)
            {
                _model.InitShader();
                _model.Draw(x_translate, y_translate, Z_translate, x_rotation, y_rotation, _width, _height);
            }
            
            // Zeby wywołać kolejną klatkę wyświetlania
            Dispatcher.UIThread.Post(InvalidateVisual, DispatcherPriority.Background);
        }

        /// <summary>
        /// Metoda deinicjalizująca widok opengl'a
        /// </summary>
        /// <param name="gl"></param>
        /// <param name="fb"></param>
        protected override void OnOpenGlDeinit(GlInterface gl, int fb)
        {
            Console.WriteLine("OpenGL Deinitialized");
        }
    }
}

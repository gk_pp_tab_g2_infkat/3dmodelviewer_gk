﻿#version 330 core

in VS_OUT {
  vec3 normal;
  vec2 texCoord;
} fs_in;

uniform sampler2D u_Texture;

uniform vec3 u_lightDir;
const vec3 u_brightColor = vec3(1, 0.8, 0.6);
const vec3 u_darkColor = vec3(0,0,0.4);

out vec4 FragColor;

void main()
{
  vec3 surfColor = vec3(texture(u_Texture, fs_in.texCoord).xyz);
  vec3 normal = normalize(fs_in.normal);
  vec3 lightDir = -normalize(u_lightDir);

  vec3 ccool = 0.25 * surfColor + u_darkColor;
  vec3 cwarm = 0.25 * surfColor + u_brightColor;
  const vec3 highlight = vec3(1.0);

  float t = (dot(-normal, lightDir) + 1.0) / 2.0;
  vec3 r = 2.0 * dot(-normal, lightDir) * normal + lightDir;
  float s = clamp(100.0 * dot(r, vec3(0.0, 0.0, 1.0)) - 98.0 ,0.0, 1.0);

  vec3 color = mix(mix(ccool, cwarm, t), highlight , s);
  //vec3 color = mix(ccool, cwarm, t);
  FragColor = vec4(color, 1);
}

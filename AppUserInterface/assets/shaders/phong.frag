﻿#version 330 core

// Blinn-Phong model

in vec3 FragPos;
in vec3 out_normal;
in vec2 out_texCoord;

out vec4 FragColor;
uniform sampler2D u_Texture;
uniform vec3 u_lightDir;
const vec3 viewPos = vec3(0.0, 0.0, -2.0);

const float ambientStrength = 0.1;
const vec3 lightColor = vec3(1.0, 1.0, 1.0);

const float specularStrength = 0.5;
const float shininess = 32;

void main()
{
    // object color
    vec3 textureColor = vec3(texture(u_Texture, out_texCoord).xyz);
    
    // ambient color
    vec3 ambient = ambientStrength * lightColor;
    
    // diffuse color
    vec3 norm = normalize(out_normal);
    vec3 lightDir = normalize(u_lightDir - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuseColor = diff * lightColor;
    
    // specular
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    vec3 halfwayDir = normalize(lightDir + viewDir);
    
    float spec = pow(max(dot(out_normal, halfwayDir), 0.0), shininess);
    vec3 specular = specularStrength * spec * lightColor;
    
    // setting results
    vec3 result = (ambient + diffuseColor + specular) * textureColor;
    FragColor = vec4(result, 1.0);
}


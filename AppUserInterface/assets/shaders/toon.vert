﻿#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec3 normal;

out VS_OUT {
    vec3 normal;
    vec2 texCoord;
    vec3 FragPos;
} vs_out;

// toon shading

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main()
{
    gl_Position = proj * view * model * vec4(aPos, 1.0);
    vs_out.normal = normal; //mat3(transpose(inverse(model))) * normal;
    vs_out.texCoord = texCoord;
    vs_out.FragPos = gl_Position.xyz;
}


﻿#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoord;

out vec3 out_normal;
out vec2 out_texCoord;
out vec3 FragPos;

// oswietlenie phong'a

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main()
{
    gl_Position = proj * view * model * vec4(aPos, 1.0);
    out_normal = mat3(transpose(inverse(model))) * normal;
    out_texCoord = texCoord;
    FragPos = vec3(model * vec4(aPos, 1.0));
}


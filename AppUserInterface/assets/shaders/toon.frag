﻿#version 330 core

in VS_OUT {
    vec3 normal;
    vec2 texCoord;
    vec3 FragPos;
} fs_in;

uniform sampler2D u_Texture;

uniform vec3 u_lightDir;

// nie używane
uniform vec3 u_brightColor;
uniform vec3 u_darkColor;


const vec3 outlineColor = vec3(0.15);
const float litOutlineThickness = .022;
const float unlitOutlineThickness = .025;
const vec3 viewPos = vec3(0,0, -2.0);

out vec4 FragColor;

void main()
{
    vec3 litColor = vec3(texture(u_Texture, fs_in.texCoord).xyz);
    vec3 color = litColor * 0.40;
    vec3 normal = normalize(fs_in.normal);
    vec3 lightDir = normalize(u_lightDir - fs_in.FragPos);
    float nldot = dot(normal, lightDir);

    if (nldot >= 0.75)  {
        color = litColor;
    } else if (nldot >= 0.50)  {
        color = litColor * 0.8;
    } else if (nldot >= 0.25)  {
        color = litColor * 0.6;
    } else if (nldot >= 0.10)  {
        color = litColor * 0.45;
    }


    /*
    if (dot(viewDir, normal) < mix(unlitOutlineThickness, litOutlineThickness,
        min(1.0, nldot))) {
      color = outlineColor;
    }*/

    FragColor = vec4(color, 1.0);

}


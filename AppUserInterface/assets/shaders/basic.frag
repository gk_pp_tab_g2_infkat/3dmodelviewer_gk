﻿#version 330 core

in VS_OUT {
    vec3 normal;
    vec2 texCoord;
} fs_in;

uniform sampler2D u_Texture;
uniform vec3 u_lightDir; 


uniform vec3 u_brightColor;
uniform vec3 u_darkColor;

out vec4 FragColor;

void main()
{ 
  vec3 color = texture(u_Texture, texCoord) * dot(fs_in.normal, u_lightDir);
  color = clamp(color, 0.1 ,1.0);
  
  FragColor = vec4(color, 1);
}

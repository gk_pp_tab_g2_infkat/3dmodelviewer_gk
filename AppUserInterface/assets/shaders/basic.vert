﻿#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoord;

out VS_OUT {
    vec3 normal;
    vec2 texCoord;
} vs_out;

// proste oswietlenie

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main()
{
    gl_Position = proj * view * model * vec4(aPos, 1.0);
    vs_out.normal = normal;
    vs_out.texCoord = texCoord;
}


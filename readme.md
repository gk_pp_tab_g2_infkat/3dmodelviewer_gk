#### Katowice, 25.03.2022
<br>
<br>
<br>

# Dokument Projektowy z Grafiki Komputerowej

<br>

## Program do obsługi i edycji modeli 3D

<br><br><br><br><br><br><br><br><br>

### Informatyka Katowice Grupa 2 Sekcja 2 Zespół 2

### Skład sekcji: 
* Daniel Świetlik
* Jan Słowik
* Kamil Tlałka
* Aleksander Mickiewicz
* Tomasz Stefaniak

<br>
<br>

## 1 Treść tematu projektu

Tematem projektu jest program pozwalający na odczytanie oraz odpowiednie wyświetlanie, wraz z dodatkowymi opcjami pozwalającymi na przejrzenie jak model może się zachowywać przy np. zmiane światła czy jak będzie wyglądał z podaną teksturą, pozwoli też na edycję a później zapisanie plików modeli 3d w formacie .OBJ i dodatkowego pliku .MTL dla nałożonych tekstur lub kolorów. 
<br>
Aplikacja będzie stworzona w formie cross-platformowej aplikacji desktopowej działającej na systemach Windows oraz macOS. Program będzie pozwalał na bezproblemową kompilację bez względu na korzystaną platformę i nie wymaga instalacji dodatkowego oprogramowania jakoże korzysta tylko z systemowych rozwiązań w backendzie, m.in. OpenGL, który pozwala na bezproblemowe działanie na wielu platformach.
Program będzie posiadał przejrzysty interfejs użytkownika pozwalający na intuicyjne korzystanie z niego dla przyspieszenia pracy z nim.
<br>
Aplikacja będzie pozwalała m.in. na:

- Załadowanie modeli w formacji .OBJ,
- Prosta manipulacja modelem (rozciąganie, pomniejszanie, powiększanie, przemieszczanie wierzchołków itp.)
- Wyświetlanie modeli w 3 różnych trybach: Wireframe, nieoteksturowany, oteskturowany
- Możliwość nałożenia tekstury na model w celu podglądu efektu końcowego w innej aplikacji
- Przemieszczanie, obracanie, przybliżanie i oddalanie kamery od/do załadowanego modelu
- Dodanie oświetlenia przy wyświetlaniu modelu by sprawdzić jego wygląd w takiej sytuacji
- Kolorowanie modeli
- Zapisanie modelu do pliku .OBJ po dokonanych zmianach w modelu
- Oraz zapisanie pliku typu .MTL dla zastosowanych w celu zapisania dodatkowych informacji o teksturze i kolorach.




## 2 Analiza zadania

### Wstęp teoretyczny do zadania

Podstawową techniką, która zostanie użyta jest zastosowanie przekształceń afinicznych, które zostaną użyte do manipulacji wierzchołkami modelu. 
Przekształcenia afiniczne są to przekształcenia wzajemnie jednoznaczne z prostej w prostą, z punktu w punkt i z płaszczyzny w płaszczyznę. Przkształcenia te zachowują równoległość odcinków jednak nie zachowują równości kątów czy długości tych odcinków. Transformacje te obejmują m.in. przesunięcie równoległe, obrót czy symetrię osiową.
Przekształceń dokonuje się poprzez mnożenie macierzy przekształceń z wektorem wierzchołka, należy tutaj pamiętać o kolejności mnożenia macierzy by przekształcenia były poprawne.
<br>
Kolejną wykorzystaną techniką będzie użycie modelu oświetlenia na podstawie algorytmu modelu Phong'a, która aproksymuje światło. W równaniu tym do obliczenia wartości koloru wynikowego korzysta się z następujących elementów koloru światła:

* Światło ambient - światło pochodzące od otoczenia odbite od innych obiektów na scenie
* Światło diffuse - światło rozproszone wzależności od typu materiału
* Światło specular - światło będące odbłyskiem od obiektu źródła światła.

Model Phong'a przedstawiany jest następujący wzorem obliczającym docelowy kolor wierzchołka dla wielu źródeł światła:
$$I_{\lambda} = I_{a\lambda}k_{a}O_{d\lambda} + \sum_{1<=i<=m}{f_{att_i}I_{p\lambda i}[k_dO_{d\lambda}(\bar{N}*\bar{L})+ k_s(\bar{R_i}*\bar{V})^n]},$$
gdzie $\bar{N}$ - wektor normalny do powierzchni, $\bar{R}$ i $\bar{V}$ - wektory odbicia, $\bar{L}$ - wektor od źródła światła, $I$ - natężenie światła, $k$ - współczynnik odbicia od materiału, $f_{att}$ - współczynnik tłumienia źródła światła, $O$ - współczynnik koloru odbicia rozproszonego, $I$ - współczynnik koloru, $n$ - wykładnik odbicie źródlanego.
Powyższe równanie stosujemy dla każdej składowej koloru (RGB) osobno wtedy $\lambda$ przyjmuje odpowiednio indeks $R$, $G$, $B$.
<br>
W punktach poniżej wymienione zostaną pozostałe wykorzystane zagadnienia jak i biblioteki z których zamierzamy skorzystać, nie wymagające teoretycznego wstępu.

### Przewidywane wykorzystywane zagadnienia grafiki komputerowej

* **API graficzne OpenGL**
* **Modele oświetlenia**
* **Przekształcenia afiniczne**
* **Eliminacja niewidocznych powierzchni**

### Wykorzystywane biblioteki oraz narzędzia programistyczne

* **MS Visual Studio / JetBrains Rider** - 2 różne IDE ze względu na pracę na różnych systemach operacyjnych t.j. macOS (Rider) i Windows (VS2022),
* **.NET Core 6 i C# 10** - język programowania
* **Framework AvaloniaUI** - framework odpowiadający za interfejs graficzny oraz binding biblioteki OpenGL z C do C#. Pozwoli nam on na przygotowania aplikacji niezależnej od systemu operacyjnego co skutkuje możliwością działania na Windows i macOS bez problemu.
* **OpenGL** - API odpowiedzialne za renderowanie grafiki.
* **SharpImage** - bibiblioteka do języka C# odpowiedzialna za obsługę plików graficznych typu .png, .jpg.
* **Blender** - w celu weryfikacji działania wczytywania modeli z plików i potencjalnie generowania przykładowych modeli do testów.

## 3 Podział pracy w zespole

Poniższa tabela prezentuje ogólny plan rozłożenia pracy w zespole podzielony na części składowe aplikacji i jej kluczowe elementy. 

Imię Nazwisko | Obowiązki | Opis
--- | --- | ---
Daniel Świetlik | Dokumentacja, Abstrakcja Silnika Rederowania z OpenGL| Przygotowanie dokumentacji do projektu oraz stworzenie kodu będącego wyższą abstrakcją na renderowanie obiektów przez OpenGL'a
Kamil Tlałka | Przygototwanie Interfejsu użytkownika i assetów| Zaprojektowanie i napisanie kodu związanego z interfejsem użytkownika i interakcją między nim a programem, przygotowanie przykładowych tekstur do prezentacji aplikacji
Jan Słowik | Logika połączenia interfejsu z modelem aplikacji, przygotowanie przykładowych plików modeli| Przygotowanie kodu kontrolera modelu związanego z logiką obsługi aplikacji oraz jej interfejsu użytkownika i przygotowanie odpowiednich plików modeli dla prezentacji aplikacji
Aleksander Mickiewicz | Tworzenie shaderów glsl i pomoc przy siliniku renderowania | Przygotowanie odpowiednich shaderów dla renderowania grafiki w języku GLSL i pomoc przy tworzeniu silinka renderowania
Tomasz Stefaniak | Kod zajmujący się logiką obsługi modeli i ich edycji | Stworzenie odpowiedniego kodu do modelu, który będzie wczytywał, edytował oraz wywoływał odpowiednie funkcję związane z silnikiem renderowania grafiki

Jest to podział ogólny, nie został podzielony na konkretne tygodnie i daty do wykonania etapu ze względu na fakt, że jest to nasza pierwsza tego typu aplikacja i nie jesteśmy pewni jak długo każde z etapów i elementów zajmuje czasu do zaprogramowania.
Gdy jeden z nas skończy swój etap całkowicie przed czasem będzie on przekierowany do pomocy przy innym etapie.

## 4 Repozytorium i narzędzie zarządzania pracą zespołu
Poniżej znajdują się link'i do naszego publicznego repozytorium na GitLabie jak i projektu na Trello:

* GitLab - repozytorium: [https://gitlab.com/gk\_pp\_tab\_g2\_infkat/3dmodelviewer\_gk](https://gitlab.com/gk_pp_tab_g2_infkat/3dmodelviewer_gk)
* Trello - link: [https://trello.com/3dmodelviewer\_infkat\_g2s2\_pods2/](https://trello.com/3dmodelviewer_infkat_g2s2_pods2/)
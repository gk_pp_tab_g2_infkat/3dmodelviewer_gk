var class_model_object =
[
    [ "ModelObject", "class_model_object.html#aa8d5a6050f1da4c57d26b8b5cd23a751", null ],
    [ "Draw", "class_model_object.html#a97d51593577bd945de25f92916e303f2", null ],
    [ "InitializeDraw", "class_model_object.html#ad39a95d33bee525ba3df72d55e082656", null ],
    [ "InitShader", "class_model_object.html#aa6849fa5d39825547e0667411dbe799d", null ],
    [ "LoadModelWithFile", "class_model_object.html#a411234cebc560541a9376b78b06f7a92", null ],
    [ "LoadShaderAssets", "class_model_object.html#a40dfd351be15891f58991df7d7a0f8a9", null ],
    [ "PutTexture", "class_model_object.html#ad2da2cbabce8eda5cd1219db42564716", null ],
    [ "Rotate", "class_model_object.html#afc35c58ac2257340d9291a5affd7ccad", null ],
    [ "SaveToObjFile", "class_model_object.html#a9def77bcaa5d2f87131e36037770ae2c", null ],
    [ "Scale", "class_model_object.html#aa2cca9ec5dad4a8440af2d8ce80c9c50", null ],
    [ "SetDisplayMethod", "class_model_object.html#a09d760821258ef5a6a92c680c2b524ad", null ],
    [ "StopUsingLight", "class_model_object.html#afa90c1e4c13a7c9f62f5808a1411cd33", null ],
    [ "UseLight", "class_model_object.html#a3d9d4dff7dbce586c9d5446d9a189b91", null ],
    [ "UseShaderFile", "class_model_object.html#a91eb089d017aacb2fb54d379137df5a2", null ]
];
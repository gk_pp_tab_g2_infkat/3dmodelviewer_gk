var class_gl_interface_extention_class =
[
    [ "BindBufferRange", "class_gl_interface_extention_class.html#aff868b4a3860ea39d65e414a82a60a90", null ],
    [ "BindVertexArray", "class_gl_interface_extention_class.html#a3f6d8d70da24e0b9c0899baf828770f7", null ],
    [ "BlendFunc", "class_gl_interface_extention_class.html#a19b092f1217442f14f6dcea6f76ac1e4", null ],
    [ "BufferSubData", "class_gl_interface_extention_class.html#a25fc03225c7d3ac3ec07b9c327b1df33", null ],
    [ "DeleteVertexArray", "class_gl_interface_extention_class.html#af9af01ace7345a46b6b2a218f7a774ed", null ],
    [ "DeleteVertexArray", "class_gl_interface_extention_class.html#aaa12a1b2d2f3978713539fa3a005a8be", null ],
    [ "Ext", "class_gl_interface_extention_class.html#a499a32780724fd54f2f240ef6236829d", null ],
    [ "GenTexture", "class_gl_interface_extention_class.html#a5f29da381aa557939c3db2cca1ad9304", null ],
    [ "GenVertexArray", "class_gl_interface_extention_class.html#a6c5c674bc965e02056988d70f58b1c5c", null ],
    [ "GenVertexArrays", "class_gl_interface_extention_class.html#a4e757ae7cba2d9e8d9598065e71c8226", null ],
    [ "GetUniformBlockIndex", "class_gl_interface_extention_class.html#add41c7c8160d493d631ea7bf0af2f5f0", null ],
    [ "PolygonMode", "class_gl_interface_extention_class.html#a9234da42f5b85b10bee85fea83c4b2e0", null ],
    [ "Uniform1i", "class_gl_interface_extention_class.html#a94fd2c8b51c6481489409eff6b23ed9f", null ],
    [ "Uniform3f", "class_gl_interface_extention_class.html#a0f7eac330575fa2d916248bd922537c3", null ],
    [ "Uniform4f", "class_gl_interface_extention_class.html#a585c7e8e379e789df28c426eb9ed040d", null ],
    [ "UniformBlockBinding", "class_gl_interface_extention_class.html#a190996f8109d56e1843b2150cb145ffe", null ]
];
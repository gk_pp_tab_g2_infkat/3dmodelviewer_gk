var class_app_user_interface_1_1_models_1_1_model_manipulate_model =
[
    [ "DisplayMode", "class_app_user_interface_1_1_models_1_1_model_manipulate_model.html#a32216ec269921a4d6dc410b36e845f5c", null ],
    [ "LightXPos", "class_app_user_interface_1_1_models_1_1_model_manipulate_model.html#ac28ef66eb775ea9cd17f2eb5b05a4eef", null ],
    [ "LightYPos", "class_app_user_interface_1_1_models_1_1_model_manipulate_model.html#a164ea80d5f6b14bc641824f8a02cdbdb", null ],
    [ "LightZPos", "class_app_user_interface_1_1_models_1_1_model_manipulate_model.html#a33c3bf555b8b2dfdf15503c478ab4c6a", null ],
    [ "RotateValue", "class_app_user_interface_1_1_models_1_1_model_manipulate_model.html#a410249cdd1de4eecd5d0413f5483a0b2", null ],
    [ "ScaleValue", "class_app_user_interface_1_1_models_1_1_model_manipulate_model.html#ae08bbfa4bf002a3349322d38c70df1a0", null ],
    [ "UseLight", "class_app_user_interface_1_1_models_1_1_model_manipulate_model.html#a65cec23b82fa1488c344a7f0845bb58a", null ]
];
var indexSectionsWithContent =
{
  0: "_abcdefghiklmnoprstuvwxyz",
  1: "aefgimoprstuvz",
  2: "amv",
  3: "abdegmoprstuv",
  4: "abcdefgilmnoprstuvwz",
  5: "_cegimnprv",
  6: "gptv",
  7: "dr",
  8: "nstw",
  9: "bcdefghilmnoprstuvwxyz",
  10: "dlprst",
  11: "k"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "properties",
  10: "events",
  11: "pages"
};

var indexSectionLabels =
{
  0: "Wszystko",
  1: "Klasy",
  2: "Przestrzenie nazw",
  3: "Pliki",
  4: "Funkcje",
  5: "Zmienne",
  6: "Definicje typów",
  7: "Wyliczenia",
  8: "Wartości wyliczeń",
  9: "Właściwości",
  10: "Zdarzenia",
  11: "Strony"
};


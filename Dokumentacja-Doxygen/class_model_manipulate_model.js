var class_model_manipulate_model =
[
    [ "DisplayMode", "class_model_manipulate_model.html#a1e507375348d73bd913aec4a06cbdc25", null ],
    [ "LightXPos", "class_model_manipulate_model.html#a76e4c56b3ec0c2e8fa8bb599c67c3848", null ],
    [ "LightYPos", "class_model_manipulate_model.html#a45f631edba159445d383f8a79c5a2c7a", null ],
    [ "LightZPos", "class_model_manipulate_model.html#aa13074be4d5b4f7e89cece50a8e9ce85", null ],
    [ "RotateValue", "class_model_manipulate_model.html#af47324eadd93208c4b82b6fea48c23ba", null ],
    [ "ScaleValue", "class_model_manipulate_model.html#a414f7cfa18df0362d10a37f7619cf3a4", null ],
    [ "UseLight", "class_model_manipulate_model.html#a3687e1b65866769de3a1e16415f86369", null ]
];
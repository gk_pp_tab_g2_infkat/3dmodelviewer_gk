var class_model_manipulate_view_model =
[
    [ "ModelManipulateViewModel", "class_model_manipulate_view_model.html#a615d6c9400e02865df59b8e7ae4a7f5c", null ],
    [ "OnPropertyChanged", "class_model_manipulate_view_model.html#a46d1e7e0a0c69a6ff9d089b5a9951ce1", null ],
    [ "_model", "class_model_manipulate_view_model.html#a4710d1782a7902237480e0110c375c9e", null ],
    [ "CheckedDisplayMode", "class_model_manipulate_view_model.html#ab9f80f74a2fca8a788ce54986bc565ee", null ],
    [ "LightXPos", "class_model_manipulate_view_model.html#a042be0b7003eb8d810054afbdfc0f96c", null ],
    [ "LightYPos", "class_model_manipulate_view_model.html#add2d1008b0953cf60ec97a9b890a9744", null ],
    [ "LightZPos", "class_model_manipulate_view_model.html#aead1b102c0296408e8fbd03a762c88d2", null ],
    [ "RotateValue", "class_model_manipulate_view_model.html#ae5563c69f3653d2b81e41fa60787755b", null ],
    [ "ScaleValue", "class_model_manipulate_view_model.html#a46729519c31c0ed310f05d098572461d", null ],
    [ "UseLight", "class_model_manipulate_view_model.html#a5a6a8db4bfadcfd1546275cc329a86c9", null ],
    [ "PropertyChanged", "class_model_manipulate_view_model.html#afac0274170a7fffa6c124cb457b7e9ef", null ]
];
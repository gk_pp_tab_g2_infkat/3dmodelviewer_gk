var namespace_app_user_interface =
[
    [ "Models", "namespace_app_user_interface_1_1_models.html", "namespace_app_user_interface_1_1_models" ],
    [ "Viewmodels", "namespace_app_user_interface_1_1_viewmodels.html", "namespace_app_user_interface_1_1_viewmodels" ],
    [ "Views", "namespace_app_user_interface_1_1_views.html", "namespace_app_user_interface_1_1_views" ],
    [ "App", "class_app_user_interface_1_1_app.html", "class_app_user_interface_1_1_app" ],
    [ "MainWindow", "class_app_user_interface_1_1_main_window.html", "class_app_user_interface_1_1_main_window" ],
    [ "ModelManipulateView", "class_app_user_interface_1_1_model_manipulate_view.html", "class_app_user_interface_1_1_model_manipulate_view" ],
    [ "ModelViewControl", "class_app_user_interface_1_1_model_view_control.html", "class_app_user_interface_1_1_model_view_control" ],
    [ "PlatformIncorrectForThisMethodException", "class_app_user_interface_1_1_platform_incorrect_for_this_method_exception.html", "class_app_user_interface_1_1_platform_incorrect_for_this_method_exception" ],
    [ "PlatformUtils", "class_app_user_interface_1_1_platform_utils.html", "class_app_user_interface_1_1_platform_utils" ],
    [ "Program", "class_app_user_interface_1_1_program.html", "class_app_user_interface_1_1_program" ]
];
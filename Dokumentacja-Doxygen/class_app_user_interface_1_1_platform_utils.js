var class_app_user_interface_1_1_platform_utils =
[
    [ "IsLinux", "class_app_user_interface_1_1_platform_utils.html#a0ad9ea56581548032228abfdae1b43f6", null ],
    [ "IsMacOS", "class_app_user_interface_1_1_platform_utils.html#a451a2c231a90f38dbe4a2869465d1407", null ],
    [ "IsRetinaDisplay", "class_app_user_interface_1_1_platform_utils.html#a6321d9566b359171d005d58cc78ae96a", null ],
    [ "IsWindows", "class_app_user_interface_1_1_platform_utils.html#a078956f089e61f05ef3b3a1da438d1fe", null ],
    [ "RunShellCommand", "class_app_user_interface_1_1_platform_utils.html#a4a1ee97439d608b0792895f2594feee5", null ],
    [ "RunUnixShellCommand", "class_app_user_interface_1_1_platform_utils.html#a49274251e5585173d599ff368012558e", null ],
    [ "RunWindowsShellCommand", "class_app_user_interface_1_1_platform_utils.html#a1c364512d25aab98d89ab198c5003e09", null ]
];
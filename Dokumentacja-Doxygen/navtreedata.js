/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "3D Obj Model Viewer", "index.html", [
    [ "Katowice, 25.03.2022", "md___users_daniel_programowanie__studia__projekt__grafika__model3_d_viewer_readme_18_46_14.html", [
      [ "Dokument Projektowy z Grafiki Komputerowej", "md___users_daniel_programowanie__studia__projekt__grafika__model3_d_viewer_readme_18_46_14.html#autotoc_md1", [
        [ "Program do obsługi i edycji modeli 3D", "md___users_daniel_programowanie__studia__projekt__grafika__model3_d_viewer_readme_18_46_14.html#autotoc_md2", [
          [ "Informatyka Katowice Grupa 2 Sekcja 2 Zespół 2", "md___users_daniel_programowanie__studia__projekt__grafika__model3_d_viewer_readme_18_46_14.html#autotoc_md3", null ],
          [ "Skład sekcji:", "md___users_daniel_programowanie__studia__projekt__grafika__model3_d_viewer_readme_18_46_14.html#autotoc_md4", null ]
        ] ],
        [ "1 Treść tematu projektu", "md___users_daniel_programowanie__studia__projekt__grafika__model3_d_viewer_readme_18_46_14.html#autotoc_md5", null ],
        [ "2 Analiza zadania", "md___users_daniel_programowanie__studia__projekt__grafika__model3_d_viewer_readme_18_46_14.html#autotoc_md6", [
          [ "Wstęp teoretyczny do zadania", "md___users_daniel_programowanie__studia__projekt__grafika__model3_d_viewer_readme_18_46_14.html#autotoc_md7", null ],
          [ "Przewidywane wykorzystywane zagadnienia grafiki komputerowej", "md___users_daniel_programowanie__studia__projekt__grafika__model3_d_viewer_readme_18_46_14.html#autotoc_md8", null ],
          [ "Wykorzystywane biblioteki oraz narzędzia programistyczne", "md___users_daniel_programowanie__studia__projekt__grafika__model3_d_viewer_readme_18_46_14.html#autotoc_md9", null ]
        ] ],
        [ "3 Podział pracy w zespole", "md___users_daniel_programowanie__studia__projekt__grafika__model3_d_viewer_readme_18_46_14.html#autotoc_md10", null ],
        [ "4 Repozytorium i narzędzie zarządzania pracą zespołu", "md___users_daniel_programowanie__studia__projekt__grafika__model3_d_viewer_readme_18_46_14.html#autotoc_md11", null ]
      ] ]
    ] ],
    [ "Pakiety", "namespaces.html", [
      [ "Package List", "namespaces.html", "namespaces_dup" ],
      [ "Składowe pakietu", "namespacemembers.html", [
        [ "Wszystko", "namespacemembers.html", null ],
        [ "Wyliczenia", "namespacemembers_enum.html", null ]
      ] ]
    ] ],
    [ "Klasy", "annotated.html", [
      [ "Lista klas", "annotated.html", "annotated_dup" ],
      [ "Indeks klas", "classes.html", null ],
      [ "Hierarchia klas", "hierarchy.html", "hierarchy" ],
      [ "Składowe klas", "functions.html", [
        [ "Wszystko", "functions.html", "functions_dup" ],
        [ "Funkcje", "functions_func.html", null ],
        [ "Zmienne", "functions_vars.html", null ],
        [ "Wyliczenia", "functions_enum.html", null ],
        [ "Właściwości", "functions_prop.html", null ],
        [ "Zdarzenia", "functions_evnt.html", null ]
      ] ]
    ] ],
    [ "Pliki", "files.html", [
      [ "Lista plików", "files.html", "files_dup" ],
      [ "Składowe plików", "globals.html", [
        [ "Wszystko", "globals.html", null ],
        [ "Definicje typów", "globals_type.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_app_8axaml_8cs.html",
"class_app_user_interface_1_1_platform_utils.html#a49274251e5585173d599ff368012558e",
"class_model_render_engine_1_1_texture.html#a77a726d40070a61b2cdfb4ed0a77ad5c",
"functions_h.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';
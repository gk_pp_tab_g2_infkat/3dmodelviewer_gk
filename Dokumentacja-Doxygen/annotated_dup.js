var annotated_dup =
[
    [ "AppUserInterface", "namespace_app_user_interface.html", [
      [ "Models", "namespace_app_user_interface_1_1_models.html", [
        [ "ModelManipulateModel", "class_app_user_interface_1_1_models_1_1_model_manipulate_model.html", "class_app_user_interface_1_1_models_1_1_model_manipulate_model" ]
      ] ],
      [ "Viewmodels", "namespace_app_user_interface_1_1_viewmodels.html", [
        [ "Converters", "namespace_app_user_interface_1_1_viewmodels_1_1_converters.html", [
          [ "RadioButtonToBooleanConverter", "class_app_user_interface_1_1_viewmodels_1_1_converters_1_1_radio_button_to_boolean_converter.html", "class_app_user_interface_1_1_viewmodels_1_1_converters_1_1_radio_button_to_boolean_converter" ],
          [ "RadioButtonValueConverter", "class_app_user_interface_1_1_viewmodels_1_1_converters_1_1_radio_button_value_converter.html", "class_app_user_interface_1_1_viewmodels_1_1_converters_1_1_radio_button_value_converter" ]
        ] ],
        [ "ModelManipulateViewModel", "class_app_user_interface_1_1_viewmodels_1_1_model_manipulate_view_model.html", "class_app_user_interface_1_1_viewmodels_1_1_model_manipulate_view_model" ]
      ] ],
      [ "Views", "namespace_app_user_interface_1_1_views.html", [
        [ "ErrorMessageDialog", "class_app_user_interface_1_1_views_1_1_error_message_dialog.html", "class_app_user_interface_1_1_views_1_1_error_message_dialog" ]
      ] ],
      [ "App", "class_app_user_interface_1_1_app.html", "class_app_user_interface_1_1_app" ],
      [ "MainWindow", "class_app_user_interface_1_1_main_window.html", "class_app_user_interface_1_1_main_window" ],
      [ "ModelManipulateView", "class_app_user_interface_1_1_model_manipulate_view.html", "class_app_user_interface_1_1_model_manipulate_view" ],
      [ "ModelViewControl", "class_app_user_interface_1_1_model_view_control.html", "class_app_user_interface_1_1_model_view_control" ],
      [ "PlatformIncorrectForThisMethodException", "class_app_user_interface_1_1_platform_incorrect_for_this_method_exception.html", "class_app_user_interface_1_1_platform_incorrect_for_this_method_exception" ],
      [ "PlatformUtils", "class_app_user_interface_1_1_platform_utils.html", "class_app_user_interface_1_1_platform_utils" ],
      [ "Program", "class_app_user_interface_1_1_program.html", "class_app_user_interface_1_1_program" ]
    ] ],
    [ "ModelRenderEngine", "namespace_model_render_engine.html", [
      [ "Asset", "struct_model_render_engine_1_1_asset.html", "struct_model_render_engine_1_1_asset" ],
      [ "Assets", "class_model_render_engine_1_1_assets.html", "class_model_render_engine_1_1_assets" ],
      [ "GlInterfaceExtention", "class_model_render_engine_1_1_gl_interface_extention.html", "class_model_render_engine_1_1_gl_interface_extention" ],
      [ "GlInterfaceExtentionClass", "class_model_render_engine_1_1_gl_interface_extention_class.html", "class_model_render_engine_1_1_gl_interface_extention_class" ],
      [ "GLUtils", "class_model_render_engine_1_1_g_l_utils.html", "class_model_render_engine_1_1_g_l_utils" ],
      [ "IndexBuffer", "class_model_render_engine_1_1_index_buffer.html", "class_model_render_engine_1_1_index_buffer" ],
      [ "IUniformBlockBufferElement", "interface_model_render_engine_1_1_i_uniform_block_buffer_element.html", "interface_model_render_engine_1_1_i_uniform_block_buffer_element" ],
      [ "Renderer", "class_model_render_engine_1_1_renderer.html", "class_model_render_engine_1_1_renderer" ],
      [ "ShaderLoader", "class_model_render_engine_1_1_shader_loader.html", "class_model_render_engine_1_1_shader_loader" ],
      [ "ShaderProgram", "interface_model_render_engine_1_1_shader_program.html", "interface_model_render_engine_1_1_shader_program" ],
      [ "Texture", "class_model_render_engine_1_1_texture.html", "class_model_render_engine_1_1_texture" ],
      [ "UniformBlockBuffer", "class_model_render_engine_1_1_uniform_block_buffer.html", "class_model_render_engine_1_1_uniform_block_buffer" ],
      [ "UniformBlockBufferElement", "struct_model_render_engine_1_1_uniform_block_buffer_element.html", "struct_model_render_engine_1_1_uniform_block_buffer_element" ],
      [ "Vertex", "class_model_render_engine_1_1_vertex.html", "class_model_render_engine_1_1_vertex" ],
      [ "VertexArray", "class_model_render_engine_1_1_vertex_array.html", "class_model_render_engine_1_1_vertex_array" ],
      [ "VertexBuffer", "class_model_render_engine_1_1_vertex_buffer.html", "class_model_render_engine_1_1_vertex_buffer" ],
      [ "VertexBufferElement", "struct_model_render_engine_1_1_vertex_buffer_element.html", "struct_model_render_engine_1_1_vertex_buffer_element" ],
      [ "VertexBufferLayout", "class_model_render_engine_1_1_vertex_buffer_layout.html", "class_model_render_engine_1_1_vertex_buffer_layout" ],
      [ "VerticesArray", "class_model_render_engine_1_1_vertices_array.html", "class_model_render_engine_1_1_vertices_array" ]
    ] ],
    [ "ViewerLogicLayer", "namespace_viewer_logic_layer.html", [
      [ "FileIsNotProperObjFileException", "class_viewer_logic_layer_1_1_file_is_not_proper_obj_file_exception.html", "class_viewer_logic_layer_1_1_file_is_not_proper_obj_file_exception" ],
      [ "ModelObject", "class_viewer_logic_layer_1_1_model_object.html", "class_viewer_logic_layer_1_1_model_object" ],
      [ "ModelObjectWasNotLoadedException", "class_viewer_logic_layer_1_1_model_object_was_not_loaded_exception.html", "class_viewer_logic_layer_1_1_model_object_was_not_loaded_exception" ],
      [ "ObjFileWriter", "class_viewer_logic_layer_1_1_obj_file_writer.html", "class_viewer_logic_layer_1_1_obj_file_writer" ],
      [ "ShaderProgramWasNotLoadedException", "class_viewer_logic_layer_1_1_shader_program_was_not_loaded_exception.html", "class_viewer_logic_layer_1_1_shader_program_was_not_loaded_exception" ],
      [ "VertexDataMapper", "class_viewer_logic_layer_1_1_vertex_data_mapper.html", "class_viewer_logic_layer_1_1_vertex_data_mapper" ],
      [ "VertexMapperMathHelper", "class_viewer_logic_layer_1_1_vertex_mapper_math_helper.html", "class_viewer_logic_layer_1_1_vertex_mapper_math_helper" ],
      [ "ZipExtention", "class_viewer_logic_layer_1_1_zip_extention.html", "class_viewer_logic_layer_1_1_zip_extention" ]
    ] ]
];
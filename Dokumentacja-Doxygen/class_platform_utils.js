var class_platform_utils =
[
    [ "IsLinux", "class_platform_utils.html#a89dd996d804b9da730baa08d2b8b4d4c", null ],
    [ "IsMacOS", "class_platform_utils.html#ab9c09eebc15e6783cc299c4e98de8649", null ],
    [ "IsRetinaDisplay", "class_platform_utils.html#afb3b40bccea10bec1971e50eb0be460a", null ],
    [ "IsWindows", "class_platform_utils.html#a729500775964fb74eba223d2a9741541", null ],
    [ "RunShellCommand", "class_platform_utils.html#ad1e6ec402fe2b8dd9538d857b8c79b5d", null ],
    [ "RunUnixShellCommand", "class_platform_utils.html#a80c74e65201e130f5bee860f65074702", null ],
    [ "RunWindowsShellCommand", "class_platform_utils.html#ac28ff3c50c9c24cc42a46d0011b4a911", null ]
];
namespace ViewerLogicLayer.Tests;
using ModelRenderEngine;
using OpenTK.Mathematics;
public class VertexTest
{

    static float d1 = 10, d2 = 20;
    Vector2 v2 = new Vector2(d1, d2);
    Vector3 v3 = new Vector3(d1, d2, d2);
    Vertex testVertex = new Vertex(new Vector3(d1, d2, d2), new Vector3(d1, d2, d2), new Vector2(d1, d2));


    [Fact]
    public void Test_Vertex_X()
    { 
        Assert.Equal(10 , testVertex.Position.X);
    }
    [Fact]
    public void Test_Vertex_Y()
    {
        Assert.Equal(20, testVertex.Position.Y);
    }
    [Fact]
    public void Test_Vertex_Z()
    {
        Assert.Equal(20, testVertex.Position.Z);
    }
    [Fact]
    public void Test_Vertex_Texture()
    {
        Assert.Equal(v2, testVertex.TextureMapping.Value);
    }
    [Fact]
    public void Test_Vertex_Normal()
    {
        Assert.Equal(v3, testVertex.Normal.Value);
    }
}
public class VertexArrayTest
{

    static float d1 = 1, d2 = 2, d3 = 3;
    Vector2 v2 = new Vector2(d1, d2);
    Vector3 v3 = new Vector3(d1, d2, d3);
    Vertex testVertex = new Vertex(new Vector3(d1, d2, d3), new Vector3(d1, d2, d3), new Vector2(d1, d2));



    [Fact]
    public void Test_Vertex_Opngl_Array()
    {
        float[] float_table = new float[8];
        float_table[0] = 1;
        float_table[1] = 2;
        float_table[2] = 3;
        float_table[3] = 1;
        float_table[4] = 2;
        float_table[5] = 3;
        float_table[6] = 1;
        float_table[7] = 2;


        List<Vertex> TestList = new List<Vertex>();
        TestList.Add(testVertex);

        VerticesArray TestArray = new VerticesArray(TestList);

        Assert.Equal(float_table, TestArray.ToOpenGlArray());
    }

    [Fact]
    public void Test_Vertex_Opngl_Array_error()
    {
        List<Vertex> TestList = new List<Vertex>();
        VerticesArray TestArray = new VerticesArray(TestList);

        Assert.Throws<System.Exception>(() => TestArray.ToOpenGlArray());
    }


    }
namespace ViewerLogicLayer.Tests;
using ModelRenderEngine;
using OpenTK.Mathematics;
using ViewerLogicLayer;
using VertexObj = ObjLoader.Loader.Data.VertexData;
public class VertexDataMaperTest
{

    static float d1 = 1, d2 = 2, d3 = 3;
    VertexObj.Vertex obv1 = new VertexObj.Vertex(1, 2, 3);
    VertexObj.Normal obn1 = new VertexObj.Normal(1, 2, 3);
    VertexObj.Texture obt1 = new VertexObj.Texture(1, 2);
    Vertex testVertex = new Vertex(new Vector3(d1, d2, d3), new Vector3(d1, d2, d3), new Vector2(d1, d2));


    [Fact]
    public void Test_MapVertex()
    {
        Assert.Equal(VertexDataMapper.MapVertex(obv1, obn1, obt1).Position, testVertex.Position);
    }
    [Fact]
    public void Test_MapVerisies()
    {
        VertexObj.Vertex obv1 = new VertexObj.Vertex(1, 2, 3);
        VertexObj.Normal obn1 = new VertexObj.Normal(1, 2, 3);
        VertexObj.Texture obt1 = new VertexObj.Texture(1, 2);

        List<VertexObj.Vertex> helpl1 = new List<VertexObj.Vertex>();
        helpl1.Add(obv1);

        List<VertexObj.Normal> helpl2 = new List<VertexObj.Normal>();
        helpl2.Add(obn1);

        List<VertexObj.Texture> helpl3 = new List<VertexObj.Texture>();
        helpl3.Add(obt1);

        IEnumerable<VertexObj.Vertex> verts = helpl1;
        IEnumerable<VertexObj.Normal> normals = helpl2;
        IEnumerable<VertexObj.Texture> textures = helpl3;

        List<Vertex> testlist = new List<Vertex>();
        testlist.Add(testVertex);

        List<Vertex> testlist2 = VertexDataMapper.MapVertices(verts, normals, textures);

        Assert.Equal(testlist[0].Position ,  testlist2[0].Position);
    }
    [Fact]
    public void Test_MapVerisies_null()
    {

        IEnumerable<VertexObj.Vertex> verts = new List<VertexObj.Vertex>();
        IEnumerable<VertexObj.Normal> normals = new List<VertexObj.Normal>();
        IEnumerable<VertexObj.Texture> textures = new List<VertexObj.Texture>();

        List<Vertex> testlist2 = VertexDataMapper.MapVertices(verts, normals, textures);

        Assert.Equal(null, testlist2);
    }

}

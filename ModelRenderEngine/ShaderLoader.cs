﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Numerics;
using System.Runtime.InteropServices;
using Avalonia.OpenGL;
using OpenTK.Mathematics;
using static Avalonia.OpenGL.GlConsts;
using Vector3 = OpenTK.Mathematics.Vector3;
using Vector4 = OpenTK.Mathematics.Vector4;

namespace ModelRenderEngine
{
    /// <summary>
    /// Interfejs reprezentujacy program shaderu
    /// </summary>
    public interface ShaderProgram
    {
        /// <summary>
        /// Właściwość przechowująca ID shaderu
        /// </summary>
        public int ID { get; }
        /// <summary>
        /// Metoda bindująća program shaderu
        /// </summary>
        public void Bind();
        /// <summary>
        /// Metoda odbindująca program shaderu
        /// </summary>
        public void Unbind();
        /// <summary>
        /// Ustawia w programie shaderu uniform - Wektor 4 floatów
        /// </summary>
        /// <param name="name">Nazwa uniformu</param>
        /// <param name="values">Wartości wektora</param>
        public void SetUniform4f(string name, Vector4 values);
        /// <summary>
        /// Ustawia uniform z pojedynczą wartością typu float
        /// </summary>
        /// <param name="name">Nazwa uniformu</param>
        /// <param name="value">Wartość uniformu</param>
        public void SetUniform1f(string name, float value);
        /// <summary>
        /// Ustawia uniform z pojedynczym intem
        /// </summary>
        /// <param name="name">Nazwa uniformu</param>
        /// <param name="value">Wartość uniformu</param>
        public void SetUniform1i(string name, int value);
        /// <summary>
        /// Ustawia uniform z wektorem 3 wartości typu float
        /// </summary>
        /// <param name="name">Nazwa uniformu</param>
        /// <param name="values">Wektor 3 elementowy z wartościami</param>
        public void SetUniform3f(string name, Vector3 values);
        /// <summary>
        /// Ustawia uniform typu Matrix4
        /// </summary>
        /// <param name="name">Nazwa uniformu</param>
        /// <param name="mat">Obiekt macierzy 4x4</param>
        /// <param name="transpose">Czy na macierzy należy dokonać transpozycji</param>
        public void SetUniformMat4f(string name, ref Matrix4 mat, bool transpose = false);
        /// <summary>
        /// Pobiera indeks lokalizacji uniformu o podanej nazwie 
        /// </summary>
        /// <param name="name">Nazwa szukanego uniformu</param>
        /// <returns>Int z indekse uniformu w pamięci GPU</returns>
        public int GetUniformLocation(string name);
    }

    /// <summary>
    /// Klasa fabryki ładująca z plików shadery i tworząca obiekt typu ShaderProgram
    /// </summary>
    public class ShaderLoader
    {
        /// <summary>
        /// Struktura symbolizująca pojedynczy shader
        /// </summary>
        public struct Shader
        {
            /// <summary>
            /// Pole przechowujące ID shaderu w OpenGl'u
            /// </summary>
            public int ID;
        }

        /// <summary>
        /// Ładuje do obiektu typu Shader plik shaderu o podanym typie i ścieżce dostępu 
        /// </summary>
        /// <param name="path">Ścieżka do pliku shaderu</param>
        /// <param name="type">Typ shaderu</param>
        /// <returns>Obiekt Shader jeśli udało się załadować lub null jeśli nie</returns>
        public static Shader? LoadShader(GlInterface GL, string path, int type)
        {
            int shaderId = GL.CreateShader(type);

            try
            {
                string compilationLog = GL.CompileShaderAndGetError(shaderId, File.ReadAllText(path));
                Console.Error.WriteLine(compilationLog);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }

            return new Shader() {ID = shaderId};
        }

        /// <summary>
        /// Tworzy obiekt ShaderProgram na podstawie podanych scieżek do shadera wierzchołków if fragmentów.
        /// </summary>
        /// <param name="vertPath">Ścieżka do pliku shaderu wierzchołków</param>
        /// <param name="fragPath">Ścieżka do pliku shaderu fragmentów</param>
        /// <returns>ShaderProgram jeśli udało się załadować program lub null jeśli wystąpił jakiś błąd</returns>
        public static ShaderProgram? LoadShaderProgram(GlInterface GL, string vertPath, string fragPath)
        {
            int shaderProgramId = GL.CreateProgram();

            Shader? vertexOpt = LoadShader(GL, vertPath, GL_VERTEX_SHADER);
            Shader? fragmentOpt = LoadShader(GL, fragPath, GL_FRAGMENT_SHADER);

            if (!vertexOpt.HasValue || !fragmentOpt.HasValue)
                return null;

            Shader vertex = vertexOpt.Value;
            Shader fragment = fragmentOpt.Value;

            GL.AttachShader(shaderProgramId, vertex.ID);
            GL.AttachShader(shaderProgramId, fragment.ID);
            var error = GL.LinkProgramAndGetError(shaderProgramId);
            GL.DeleteShader(vertex.ID);
            GL.DeleteShader(fragment.ID);

            if (!string.IsNullOrEmpty(error))
            {
                Console.WriteLine(error);
                return null;
            }


            return new ShaderProgramImpl(GL, shaderProgramId);
        }

        /// <summary>
        /// Klasa implementacji interface'u ShaderProgram
        /// </summary>
        private class ShaderProgramImpl : ShaderProgram
        {
            private readonly GlInterface GL;
            private readonly int _ID;
            private readonly Dictionary<string, int> m_UniformLocationCache = new Dictionary<string, int>();

            public ShaderProgramImpl(GlInterface gl, int _id)
            {
                _ID = _id;
                GL = gl;
            }

            public int ID
            {
                get => _ID;
            }

            public void Bind()
            {
                GLUtils.ClearError(GL);
                GL.UseProgram(_ID);
                GLUtils.CheckError(GL);
            }

            public void Unbind()
            {
                GL.UseProgram(0);
            }

            public void SetUniform4f(string name, Vector4 values)
            {
                GLUtils.ClearError(GL);

                int location = this.GetUniformLocation(name);

                GL.Uniform4f(location, values.X, values.Y, values.Z, values.W);

                GLUtils.CheckError(GL);
            }

            public void SetUniform1f(string name, float value)
            {
                GLUtils.ClearError(GL);

                int location = this.GetUniformLocation(name);

                GL.Uniform1f(location, value);

                GLUtils.CheckError(GL);
            }

            public void SetUniform1i(string name, int value)
            {
                GLUtils.ClearError(GL);

                int location = this.GetUniformLocation(name);

                GL.Uniform1i(location, value);

                GLUtils.CheckError(GL);
            }

            public void SetUniform3f(string name, Vector3 values)
            {
                GLUtils.ClearError(GL);

                int location = this.GetUniformLocation(name);

                GL.Uniform3f(location, values.X, values.Y, values.Z);

                GLUtils.CheckError(GL);
            }

            public void SetUniformMat4f(string name, ref Matrix4 mat, bool transpose = false)
            {
                GLUtils.ClearError(GL);

                int location = this.GetUniformLocation(name);
                unsafe
                {
                    fixed (void* matrix_ptr = &mat.Row0.X)
                    {
                        GL.UniformMatrix4fv(location, 1, transpose, matrix_ptr);
                    }

                }

                GLUtils.CheckError(GL);
            }

            public int GetUniformLocation(string name)
            {
                if (m_UniformLocationCache.ContainsKey(name))
                    return m_UniformLocationCache[name];

                int location = GL.GetUniformLocation(_ID, Marshal.StringToHGlobalAnsi(name));
                if (location == -1)
                {
                    Console.WriteLine("Warning: Uniform does not exist!");
                }

                m_UniformLocationCache[name] = location;
                return location;
            }
        }
    }

    
    
}


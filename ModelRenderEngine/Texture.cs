﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

using Avalonia.OpenGL;
using Avalonia.Platform;
using static Avalonia.OpenGL.GlConsts;

namespace ModelRenderEngine
{
	/// <summary>
	/// Klasa reprezentująca tekture nakładaną na obiekt modelu
	/// </summary>
	public class Texture
	{
		/// <summary>
		/// Klasa odpowiedzialna za funkcjonalność OpenGL
		/// </summary>
		private readonly GlInterface GL;
		/// <summary>
		/// ID tekstury w OpenGL
		/// </summary>
		private int RendererID;
		/// <summary>
		/// Ścieżka do pliku tekstury
		/// </summary>
		private string FilePath { get; }

		/// <summary>
		/// Szerokość tekstury w pixelach
		/// </summary>
		public int Width { get; }
		/// <summary>
		/// Wysokość tekstury w pixelach
		/// </summary>
		public int Height { get; }
		
		/// <summary>
		/// Konstruktor tekstury, który ładuje ja do pamięci w karcie graficznej 
		/// </summary>
		/// <param name="gl">Obiekt odpowiedzialny za funkcje opengl'a</param>
		/// <param name="path">Ścieżka do pliku tekstury</param>
		public Texture(GlInterface gl, string path)
		{
			GL = gl;
			FilePath = path;

			Image<Rgba32> textureImg = Image.Load<Rgba32>(path);
			textureImg.Mutate(x => x.Flip(FlipMode.Vertical));

			var LocalBuffer = new List<byte>(4 * textureImg.Width * textureImg.Height);
			Width = textureImg.Width;
			Height = textureImg.Height;

			textureImg.ProcessPixelRows(rows =>
			{
				for (int i = 0; i < rows.Height; i++)
                {
					Span<Rgba32> row = rows.GetRowSpan(i);
					for (int j = 0; j < row.Length; j++)
                    {
						LocalBuffer.Add(row[j].R);
						LocalBuffer.Add(row[j].G);
						LocalBuffer.Add(row[j].B);
						LocalBuffer.Add(row[j].A);
						
                    }
					
                }
			});

			GLUtils.ClearError(GL);

			RendererID = GL.GenTexture();
			GL.BindTexture(GL_TEXTURE_2D, RendererID);

			GL.TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			GL.TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			GL.TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			GL.TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

			int size = Marshal.SizeOf(LocalBuffer[0]) * LocalBuffer.Count;
			IntPtr buffPtr = Marshal.AllocHGlobal(size);
			Marshal.Copy(LocalBuffer.ToArray(), 0, buffPtr, LocalBuffer.Count);
			
			GL.TexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, Width, Height, 0,
				GL_RGBA, GL_UNSIGNED_BYTE, buffPtr);

			GL.BindTexture(GL_TEXTURE_2D, 0);

			GLUtils.CheckError(GL);

			textureImg.Dispose();
		}
		/// <summary>
		/// Konstruktor pozwalający na utworzenie udawanej tekstury 2x2 o wybranym kolorze
		/// </summary>
		/// <param name="gl">Obiekt z funkcjami OpenGl</param>
		/// <param name="R">Wartość koloru czerwonego</param>
		/// <param name="G">Wartość koloru zielonego</param>
		/// <param name="B">Wartośc koloru niebieskiego</param>
		public Texture(GlInterface gl, byte R, byte G, byte B) 
		{
			GL = gl;
			var LocalBuffer = new List<byte>();
			LocalBuffer.AddRange(new byte[] { R, G, B, 255});
			LocalBuffer.AddRange(new byte[] { R, G, B, 255});
			LocalBuffer.AddRange(new byte[] { R, G, B, 255});
			LocalBuffer.AddRange(new byte[] { R, G, B, 255});

			GLUtils.ClearError(GL);

			RendererID = GL.GenTexture();
			GL.BindTexture(GL_TEXTURE_2D, RendererID);

			GL.TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			GL.TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			GL.TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			GL.TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

			int size = Marshal.SizeOf(LocalBuffer[0]) * LocalBuffer.Count;
			IntPtr buffPtr = Marshal.AllocHGlobal(size);
			Marshal.Copy(LocalBuffer.ToArray(), 0, buffPtr, LocalBuffer.Count);
			
			GL.TexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 2, 2, 0,
				GL_RGBA, GL_UNSIGNED_BYTE, buffPtr);

			GL.BindTexture(GL_TEXTURE_2D, 0);

			GLUtils.CheckError(GL);

		}

		/// <summary>
		/// Bindowanie tekstury
		/// </summary>
		/// <param name="slot">Nr. slotu tekstury</param>
		public void Bind(int slot)
        {
			GLUtils.ClearError(GL);

			int textureSlot = (int)GL_TEXTURE0 + slot;
			GL.ActiveTexture(textureSlot);
			GL.BindTexture(GL_TEXTURE_2D, RendererID);

			GLUtils.CheckError(GL);
		}
		/// <summary>
		/// Odbindowanie tekstury
		/// </summary>
		public void Unbind()
        {
			GL.BindTexture(GL_TEXTURE_2D, 0);
		}
	}
}


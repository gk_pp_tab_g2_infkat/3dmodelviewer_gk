﻿using System;
using Avalonia.OpenGL;
using GlErr = Avalonia.OpenGL.GlErrors ;
using static Avalonia.OpenGL.GlConsts;

namespace ModelRenderEngine
{
    /// <summary>
    /// Klasa odpowiadająca za renderowanie grafiki przy pomocy opengl'a
    /// </summary>
    public class Renderer
    {
        /// <summary>
        /// Enum symbolizujący tryb renderowania
        /// </summary>
        public enum RenderingMode
        {
            Wireframe = 0,
            Normal
        }
        
        /// <summary>
        /// Obiekt przechowujący metody z funkcjami opengl
        /// </summary>
        private readonly GlInterface GL;
        
        /// <summary>
        /// Konstruktor przyjmujący obiekt klasy z funkcjami dla opengl'a
        /// </summary>
        /// <param name="gl">Obiekt opengl'a z frameworku avalonia</param>
		public Renderer(GlInterface gl)
        {
            GL = gl;
        }

        /// <summary>
        /// Czyści odopowiednie buffery opengl'a
        /// </summary>
        public void Clear()
        {
            GL.Clear(GL_COLOR_BUFFER_BIT);
        }

        /// <summary>
        /// Metoda rysująca obiekt na ekranie przy użyciu tablicy wierzchołków opengl'a, bufora indeksów,
        /// shaderu i o podanym trybie renderingu
        /// </summary>
        /// <param name="vao">Tablica wierzchołków</param>
        /// <param name="ibo">Bufor indeksów</param>
        /// <param name="shader">Program shaderu</param>
        /// <param name="rType">Tryb renderowania</param>
        public void Draw(VertexArray vao, IndexBuffer ibo, ShaderProgram shader, RenderingMode rType)
        {
            
            shader.Bind();
            vao.Bind();
            ibo.Bind();

            switch (rType)
            {
                case RenderingMode.Wireframe:
                    GL.PolygonMode( GL_FRONT_AND_BACK, GL_LINE );
                    break;
                case RenderingMode.Normal:
                    GL.PolygonMode( GL_FRONT_AND_BACK, GL_FILL );
                    break;
            }
            
            GLUtils.ClearError(GL);
            //GL.DrawArrays(GL_TRIANGLES, 0, new IntPtr(ibo.Count));
            GL.DrawElements(GL_TRIANGLES, ibo.Count, GL_UNSIGNED_INT,  IntPtr.Zero);
            GLUtils.CheckError(GL);

            shader.Unbind();
            vao.Unbind();
            ibo.Unbind();
        }
	}

    /// <summary>
    /// Klasa do dodatkowych metod dla openGl
    /// </summary>
    public class GLUtils
    {
        /// <summary>
        /// Czyści wszystkie wyrzucone błędy opengl'a
        /// </summary>
        public static void ClearError(GlInterface GL)
        {
            while (GL.GetError() != (int) GlErr.GL_NO_ERROR) ;
        }

        /// <summary>
        /// Sprawdza wyrzucone błędy opengl'a i wypisuje to na ekranie jeśli to warrning lub wyrzuca wyjątek jeśli to błąd
        /// </summary>
        /// <returns>zwraca true jesli blad </returns>
        /// <exception cref="Exception">Gdy blad opengl'a wystapil</exception>
        public static bool CheckError(GlInterface GL)
        {
            int err;
            while ((err = GL.GetError()) != (int) GlErr.GL_NO_ERROR)
            {
                if (GL.GetString(err).StartsWith("WARNING:"))
                    Console.Error.WriteLine(err);
                else
                    throw new Exception($"[OpenGL ERROR]: {err}, {GL.GetString(err)}");
            }
            return true;
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using Avalonia.OpenGL;
using OpenTK.Mathematics;
using static Avalonia.OpenGL.GlConsts;

namespace ModelRenderEngine
{
    /// <summary>
    /// Klasa reprezentujaca bufor wierzcholkow dla opengl'a
    /// </summary>
	public class VertexBuffer
    {
        /// <summary>
        /// Id bufora w opengl'u
        /// </summary>
        public int RendererID { get; }
        /// <summary>
        /// Obiekt reprezentujacy zbior zmapowanych funkcji opengl'a
        /// </summary>
        private readonly GlInterface GL;
        
        /// <summary>
        /// Konstruktor tworzacy bufor w opengl'u i bindujacy przyjeta tablice wierzcholkow do tego bufora
        /// </summary>
        /// <param name="gl">obiekt opengl'a</param>
        /// <param name="data">wierzcholki</param>
        public VertexBuffer(GlInterface gl, float[] data)
        {
            GL = gl;
            
            unsafe
            {
                GLUtils.ClearError(GL);
                RendererID = GL.GenBuffer();
                GL.BindBuffer(GL_ARRAY_BUFFER, RendererID);
                fixed (float* ptrd = data)
                {
                    GL.BufferData(GL_ARRAY_BUFFER, new IntPtr(data.Length * sizeof(float)), new IntPtr(ptrd), GL_STATIC_DRAW);
                }
                GLUtils.CheckError(GL);
            }
        }

        /// <summary>
        /// Bindowanie bufora
        /// </summary>
        public void Bind()
        {
            GLUtils.ClearError(GL);
                GL.BindBuffer(GL_ARRAY_BUFFER, RendererID);
            GLUtils.CheckError(GL);
        }
        /// <summary>
        /// Odbindowanie bufora
        /// </summary>
        public void Unbind()
        {
            GLUtils.ClearError(GL);
                GL.BindBuffer(GL_ARRAY_BUFFER, 0);
            GLUtils.CheckError(GL);
        }
        /// <summary>
        /// Usuniecie bufora z pamieci 
        /// </summary>
        public void Delete()
        {
            GLUtils.ClearError(GL);
                GL.DeleteBuffers(1, new int[] { RendererID });
            GLUtils.CheckError(GL);
        }
    }

    /// <summary>
    /// Klasa reprezentujaca bufor indeksow
    /// </summary>
    public class IndexBuffer
    {
        /// <summary>
        /// Obiekt z funkcjami OpenGl'a
        /// </summary>
        private readonly GlInterface GL;
        /// <summary>
        /// ID bufora w opengl
        /// </summary>
        public int RendererID { get; }
        /// <summary>
        /// ilosc elementow w buforze
        /// </summary>
        public int Count { get; }
        /// <summary>
        /// Typ zmiennej w formacie OpenGl'a
        /// </summary>
        public int GLType;
        
        /// <summary>
        /// Konstruktor tworzacy bufor i bindujacy przyjmowane indeksy do niego
        /// </summary>
        /// <param name="gl"></param>
        /// <param name="indicies">tablica indeksow uint</param>
        public IndexBuffer(GlInterface gl, uint[] indicies)
        {
            GL = gl;
            GLType = GL_UNSIGNED_INT;
            Count = indicies.Length;
            unsafe
            {
                GLUtils.ClearError(GL);
                RendererID = GL.GenBuffer();
                GL.BindBuffer(GL_ELEMENT_ARRAY_BUFFER, RendererID);
               
                GLUtils.CheckError(GL);
                GLUtils.ClearError(GL);
                fixed (void* t = &indicies[0])
                {
                    GL.BufferData(GL_ELEMENT_ARRAY_BUFFER, new IntPtr(indicies.Length * sizeof(uint)),
                        new IntPtr(t), GL_STATIC_DRAW);
                }
                GLUtils.CheckError(GL);
            }

           
        }
        /// <summary>
        /// Bindowanie bufora
        /// </summary>
        public void Bind()
        {
            GLUtils.ClearError(GL);
                GL.BindBuffer(GL_ELEMENT_ARRAY_BUFFER, RendererID);
            GLUtils.CheckError(GL);
        }
        /// <summary>
        /// Odbindowanie bufora
        /// </summary>
        public void Unbind()
        {
            GLUtils.ClearError(GL);
                GL.BindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
            GLUtils.CheckError(GL);
        }
        /// <summary>
        /// Usuniecie bufora
        /// </summary>
        public void Delete()
        {
            GLUtils.ClearError(GL);
                GL.DeleteBuffers(1, new int[] { RendererID });
            GLUtils.CheckError(GL);
        }
    }

    /// <summary>
    /// Struktura reprezentujaca pojedynczy element w buforze wierzcholkow
    /// tzn. np. pozycje wierzcholkow, czy kolor
    /// </summary>
    public struct VertexBufferElement
    {
        /// <summary>
        /// Ilosc danych skladajacych sie na element
        /// </summary>
        public int Count;
        /// <summary>
        /// Czy dane te nalezy potem znormalizowac
        /// </summary>
        public bool Normalize;
        /// <summary>
        /// Typ elementow;
        /// </summary>
        public int ElementType;
        /// <summary>
        /// Rozmiar w bajtach dla pojedynczej danej w elemencie
        /// </summary>
        public int SizeOf
        {
            get
            {
                return ElementType switch
                {
                    GL_INT => sizeof(int),
                    GL_FLOAT => sizeof(float),
                    GL_BYTE => sizeof(byte),
                    GL_DOUBLE => sizeof(double),
                    GL_UNSIGNED_INT => sizeof(uint),
                    _ => 0
                };
            }
        }
    }

    /// <summary>
    /// Klasa reprezentujaca rozlozenie elementów w buforze wierzcholkow
    /// Np. layout sklada sie na 3 elementy reprezentujace pozycje wierzcholkow, kolor i mapowanie tekstur
    /// </summary>
    public class VertexBufferLayout
    {
        /// <summary>
        /// Lista elementow
        /// </summary>
        public List<VertexBufferElement> Elements { get;  }
        /// <summary>
        /// Przesuniecie wzgledem kolejnego wierzcholka
        /// </summary>
        public int Stride { get; private set; }

        /// <summary>
        /// Konstruktor domyslny
        /// </summary>
        public VertexBufferLayout()
        {
            Elements = new List<VertexBufferElement>();
            Stride = 0;
        }
        /// <summary>
        /// Metoda dodajaca element do listy
        /// </summary>
        /// <param name="gltype">typ elementu</param>
        /// <param name="count">ilosc elementow</param>
        /// <param name="normalized">czy te elementy nalezy normalizowac</param>
        public void Push(int gltype, int count, bool normalized = false)
        {
            
            var vbe = new  VertexBufferElement()
            {
                Count = count,
                Normalize = normalized,
                ElementType = gltype
            };

            Elements.Add(vbe);

            Stride += vbe.SizeOf * count;
        }


    }

    /// <summary>
    /// Klasa reprezentujaca tablice wierzcholkow
    /// </summary>
    public class VertexArray
    {
        /// <summary>
        /// Obiekt z funkcjami OpenGl'a
        /// </summary>
        private readonly GlInterface GL;
        /// <summary>
        /// Id tablicy w opengl
        /// </summary>
        public int RendererID { get; }

        /// <summary>
        /// Konstruktor przyjmujacy obiekt opengl'a
        /// </summary>
        /// <param name="gl"></param>
        public VertexArray(GlInterface gl)
        {
            GL = gl;
            RendererID = GL.GenVertexArray();
        }
        /// <summary>
        /// Metoda dodajaca do tablicy bufor wierzcholkow wraz z jego layoutem
        /// </summary>
        /// <param name="vb">Bufor wierzcholkow</param>
        /// <param name="layout">Layout bufora wierzcholkow</param>
        public void AddBuffer(VertexBuffer vb, VertexBufferLayout layout)
        {
            GLUtils.ClearError(GL);

            Bind();
            vb.Bind();

            List<VertexBufferElement> elements = layout.Elements;
            int offset = 0;

            for (int i = 0; i < elements.Count; i++)
            {
                VertexBufferElement elm = elements[i];
                GL.VertexAttribPointer(i, elm.Count, elm.ElementType, elm.Normalize ? 1 : 0, layout.Stride, new IntPtr(offset));
                GL.EnableVertexAttribArray(i);
                offset += elm.Count * elm.SizeOf;
            }

            GLUtils.CheckError(GL);
        }
        /// <summary>
        /// Bindowanie tablicy
        /// </summary>
        public void Bind()
        {
            GLUtils.ClearError(GL);
            GL.BindVertexArray(RendererID);
            GLUtils.CheckError(GL);
        }
        /// <summary>
        /// Odbindowanie tablicy
        /// </summary>
        public void Unbind()
        {
            GLUtils.ClearError(GL);
            GL.BindVertexArray(0);
            GLUtils.CheckError(GL);
        }
        /// <summary>
        /// Usuniecie tablicy z pamieci
        /// </summary>
        public void Delete()
        {
            GLUtils.ClearError(GL);
            GL.DeleteVertexArray(RendererID);
            GLUtils.CheckError(GL);
        }
    }

    #region Unused

    /// <summary>
    /// Interfejs dla UniformBlockBufferElement
    /// </summary>
    public interface IUniformBlockBufferElement
    {
        string Name { get; set; }
        Type ElementType { get; }
        int SizeOf { get; }
        object ElementValue { get; }
    }
    
    /// <summary>
    /// Klasa elementu do dodaniu elementu do UniformBlockBuffera
    /// </summary>
    /// <typeparam name="T">Typ podawanego elementu np. Vector4</typeparam>
    public struct UniformBlockBufferElement<T> : IUniformBlockBufferElement
    {
        public string Name { get; set; }
        public T Value { get; set; }
        
        public Type ElementType => typeof(T);
        public object ElementValue => Value;
        public int SizeOf => Marshal.SizeOf(default(T));
    }
    
    /// <summary>
    /// Klasa reprezentujaca block buffera uniformow
    /// </summary>
    public class UniformBlockBuffer
    {
        public int RendererID { get; private set; }
        public int BlockIndex { get; private set; }
        public string Name { get; private set; }
        public int MemSize { get; private set; }
        public int Offset { get; private set; }
        public bool Saved { get; private set; }
        
        // Elementy bloku uniformu
        private List<IUniformBlockBufferElement> _elements = new();
        // Interfejs opengl'a
        private GlInterface GL;
        // program shaderu, ktorego uniform dotyczy
        private ShaderProgram _shader;
        
        public UniformBlockBuffer(GlInterface gl, string name, ShaderProgram shader)
        {
            GL = gl;
            _shader = shader;
            
            Name = name;
            MemSize = 0;
            Offset = 0;
            Saved = false;

            BlockIndex = GL.GetUniformBlockIndex(_shader.ID, name);
            GL.UniformBlockBinding(_shader.ID, BlockIndex, 0);

            RendererID = GL.GenBuffer();
        }
        
        /// <summary>
        /// Dodanie nowej zmiennej do bloku 
        /// </summary>
        /// <param name="element">Element do dodania</param>
        /// <typeparam name="T"></typeparam>
        public void Push<T>(UniformBlockBufferElement<T> element)
        {
            _elements.Add(element);
            MemSize += element.SizeOf;
        }
        
        /// <summary>
        /// Zapisanie buffera
        /// </summary>
        public void SaveBuffer()
        {
            GL.BindBuffer(GL_UNIFORM_BUFFER, RendererID);
            GL.BufferData(GL_UNIFORM_BUFFER, new IntPtr(MemSize), IntPtr.Zero, GL_STATIC_DRAW);
            GL.BindBuffer(GL_UNIFORM_BUFFER, 0);
            
            GL.BindBufferRange(GL_UNIFORM_BUFFER, 0, RendererID, new IntPtr(0), new IntPtr(MemSize));

            Saved = true;
        }
        
        /// <summary>
        /// Bindowanie buffora
        /// </summary>
        /// <exception cref="Exception"></exception>
        public void Bind()
        {
            if (!Saved)
                throw new Exception("Cannot bind buffer if it was not saved!\nUse Save method");
            
            foreach (var element in _elements)
            {
                // To moze nie dzialac, nie jestem pewien
                var gcHandle = GCHandle.Alloc(element.ElementValue, GCHandleType.WeakTrackResurrection);
                IntPtr dataPtr = GCHandle.ToIntPtr(gcHandle);
                
                GL.BindBuffer(GL_UNIFORM_BUFFER, RendererID);
                GL.BufferSubData(GL_UNIFORM_BUFFER, new IntPtr(Offset), 
                    new IntPtr(element.SizeOf), dataPtr);
                GL.BindBuffer(GL_UNIFORM_BUFFER, 0);
                Offset += element.SizeOf;
            }
        }
        
        public void Unbind()
        {
            if (!Saved)
                throw new Exception("Cannot Unbind buffer if layout was not saved!\nUse Save method");
        }
    }

    #endregion

    
}


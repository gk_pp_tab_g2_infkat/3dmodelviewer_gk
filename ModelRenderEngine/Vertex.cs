﻿using System;
using System.Collections;
using System.Net;
using System.Runtime.InteropServices;
using Microsoft.VisualBasic;
using OpenTK.Mathematics;

namespace ModelRenderEngine
{
	/// <summary>
	/// Klasa, która odpowiada za reprezentacje pojedynczego wierzcholka obiektu
	/// </summary>
	public class Vertex
	{
		/// <summary>
		/// Reprezentuje wektor 3d pozycji wierzcholka
		/// </summary>
		private Vector3 _position;
		/// <summary>
		/// Reprezentuje wektor 2d dla mapowania tekstury
		/// </summary>
		private Vector2? _textures;
		/// <summary>
		/// Reprezentuje wektor 3d dla normalnych wierzcholka
		/// </summary>
		private Vector3? _normals;
		
		/// <summary>
		/// Getter dla pozycji
		/// </summary>
		public Vector3 Position
		{
			get => _position;
		}
		/// <summary>
		/// Getter i setter dla tekstur
		/// </summary>
		public Vector2? TextureMapping
		{
			get => _textures;
			set
			{
				_textures = value;
			}
		}
		/// <summary>
		/// Getter i setter dla normalnych 
		/// </summary>
		public Vector3? Normal
		{
			get => _normals;
			set
			{
				_normals = value;
			}
		}
		/// <summary>
		/// Konstruktor przyjmujacy pozycje, normalne oraz mapowanie tekstur
		/// </summary>
		/// <param name="pos">wektor pozycji</param>
		/// <param name="normal">wektor normalnych</param>
		/// <param name="text">wektor mapowania tekstur</param>
		public Vertex(Vector3 pos, Vector3? normal, Vector2? text)
		{
			_position = pos;
			_normals = normal;
			_textures = text;
		}
	}

	/// <summary>
	/// Reprezentuje liste wszystkich wierzcholkow dla obiektu
	/// </summary>
	public class VerticesArray
	{
		/// <summary>
		/// Lista wierzcholkow
		/// </summary>
		public List<Vertex> Vertices { get; set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="vertices"></param>
		public VerticesArray(List<Vertex> vertices)
		{
			Vertices = vertices;
		}

		/// <summary>
		/// Zwraca tablice typu float, ktora potem mozna przekazac do metod zwiazanych z rendererem
		/// </summary>
		/// <returns>tablice floatow</returns>
		/// <exception cref="Exception">Gdy lista wierzcholkow jest pusta</exception>
		public float[] ToOpenGlArray()
		{
			if (!Vertices.Any())
				throw new Exception("Empty list!");
			
			var arrayLen = Vertices.Count() * (3 + (Vertices[0].TextureMapping is not null ? 2 : 0) +
			               (Vertices[0].Normal is not null ? 3 : 0));
			//arrayLen = Vertices.Count() * (3+2+3);
			var verticesGlArray = new float[arrayLen];
			int glArrayIdx = 0;
			for (var i = 0; i < Vertices.Count; i++)
			{
				var vert = Vertices[i];
				var vertexList = new List<float>();
				
				vertexList.AddRange(new float[] {vert.Position.X, vert.Position.Y, vert.Position.Z});

				//vertexList.AddRange(new float[] { 0, 0, 0, 0, 0 });
				if (vert.Normal is Vector3 norm)
					vertexList.AddRange(new float[] {norm.X, norm.Y, norm.Z});
				
				if (vert.TextureMapping is Vector2 text)
					vertexList.AddRange(new float[] {text.X, text.Y});

				Array.Copy(vertexList.ToArray(), 0 , verticesGlArray, glArrayIdx, vertexList.Count);
				glArrayIdx += vertexList.Count;
			}

			return verticesGlArray;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="glArray"></param>
		/// <param name="hasNormals"></param>
		/// <param name="hasTextureMapping"></param>
		/// <returns></returns>
		public static VerticesArray Of(float[] glArray, bool hasNormals, bool hasTextureMapping)
		{
			var vertsList = new List<Vertex>();
			for (var i = 0; i < glArray.Length;)
			{
				var vert = new Vertex(new Vector3(glArray[i++], glArray[i++], glArray[i++]), null, null);
				
				if (hasNormals)
					vert.Normal = new Vector3(glArray[i++], glArray[i++], glArray[i++]);
				
				if (hasTextureMapping)
					vert.TextureMapping = new Vector2(glArray[i++], glArray[i++]);
				
				vertsList.Add(vert);
			}
			return new VerticesArray(vertsList);
		} 
 	}
}


﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ModelRenderEngine
{	
	/// <summary>
	/// Struktóra reprezentująca pojedynczy asset
	/// </summary>
	public struct Asset
    {
	    /// <summary>
	    /// Id assetu
	    /// </summary>
		public int _ID;
	    /// <summary>
	    /// Nazwa assetu
	    /// </summary>
		public string Name;
	    /// <summary>
	    /// Ścieżka do assetu
	    /// </summary>
		public string Path;
		/// <summary>
		/// Konstruktor z parametrami tworzący nową wartośc assetu
		/// </summary>
		/// <param name="id">Id assetu</param>
		/// <param name="name">Nazwa assetu</param>
		/// <param name="path">Ścieżka do pliku assetu</param>
		public Asset(int id, string name, string path)
		{
			_ID = id;
			Name = name;
			Path = path;
		}
    }
	/// <summary>
	/// Klasa reprezentujaca zestaw assetów
	/// </summary>
	public class Assets
	{
		/// <summary>
		/// Lista assetów
		/// </summary>
		private List<Asset> _listOfAssets;
		/// <summary>
		/// Liczba assetów w zbiorze
		/// </summary>
		public int Count { get; private set; }
		
		/// <summary>
		/// Konstruktor domyślny
		/// </summary>
		public Assets()
		{
			_listOfAssets = new List<Asset>();
			Count = 0;
		}
		/// <summary>
		/// Metoda dodająca nowy asset
		/// </summary>
		/// <param name="newAsset">obiekt nowego assetu</param>
		public void NewAsset(Asset newAsset)
		{
			_listOfAssets.Add(newAsset);
			Count++;
		}
		/// <summary>
		/// Metoda pobierająca asset przez jego nazwe
		/// </summary>
		/// <param name="name">Nazwa assetu</param>
		/// <returns>Asset lub null</returns>
		public Asset? GetAsset(string name)
		{
			List<Asset> assetsFiltered = _listOfAssets.FindAll(x => x.Name == name);

			if (assetsFiltered.Count == 0)
				return null;
			else
				return assetsFiltered[0];
		}
		/// <summary>
		/// Metoda pobierająca asset przez jego id
		/// </summary>
		/// <param name="id">id assetu</param>
		/// <returns>Asset lub null</returns>
		public Asset? GetAsset(int id)
		{
			List<Asset> assetsFiltered = _listOfAssets.FindAll(x => x._ID == id);

			if (assetsFiltered.Count == 0)
				return null;
			else
				return assetsFiltered[0];
		}
		/// <summary>
		/// Przeciżżony operator [] do pobierania assetu przez id
		/// </summary>
		/// <param name="id">id assetu</param>
		/// <returns>Asset lub null</returns>
		public Asset? this[int id]
		{
			get => GetAsset(id);
			private set { }
		}
		/// <summary>
		/// Przeciążony operator [] do pobierania assetu przez jego nazwe
		/// </summary>
		/// <param name="name">Nazwa assetu</param>
		/// <returns>Assetu lub null</returns>
		public Asset? this[string name]
        {
			get => GetAsset(name);
			private set { }
        }
	}
}


using System.Runtime.InteropServices;
using Avalonia.OpenGL;
using Avalonia.Platform.Interop;
using static Avalonia.OpenGL.GlConsts;

namespace ModelRenderEngine
{
    
    /// <summary>
    /// Klasa rozszerzajaca klase, ktora reprezentuje dostep do funkcji statycznych zmapowanych z opengl'a
    /// </summary>
    public static class GlInterfaceExtentionClass
    {
        /// <summary>
        /// Metoda dająca dostęp do klasy z rozszerzonymi funkcjami OpenGL'a, których brakowalo w bibliotece, z której korzysta projekt
        /// </summary>
        /// <param name="gl"></param>
        /// <returns>Obiekt klasy GlInterfaceExtension</returns>
        public static GlInterfaceExtention Ext(this GlInterface gl)
        {
            return new GlInterfaceExtention(gl);
        }
        /// <summary>
        /// Metoda do bindowania tablicy wierzchołków
        /// </summary>
        /// <param name="gl"></param>
        /// <param name="array">id talicy wierzchołków</param>
        public static void BindVertexArray(this GlInterface gl, int array)
        {
            gl.Ext().BindVertexArray(array);
        }
        /// <summary>
        /// Metoda tworząca tablicę wierzchołków i zwracająca ich id w podanej tablicy
        /// </summary>
        /// <param name="gl"></param>
        /// <param name="size">Ilość tablic do wygenerowania</param>
        /// <param name="arrays">Obiekt tablicy do zapisania id utworzonych tablic</param>
        public static void GenVertexArrays(this GlInterface gl, int size, int[] arrays)
        {
            gl.Ext().GenVertexArrays(size, arrays);
        }
        /// <summary>
        /// Metoda generująca pojedyńczą tablicę wierzchołków 
        /// </summary>
        /// <param name="gl"></param>
        /// <returns>id utworzonej tablicy</returns>
        public static int GenVertexArray(this GlInterface gl) => gl.Ext().GenVertexArray();
        /// <summary>
        /// Metoda usuwająca tablice wierzchołków o podanych id
        /// </summary>
        /// <param name="gl"></param>
        /// <param name="count">Ilość tablic do usunięcia</param>
        /// <param name="arrays">Tablica przechowująca id tablic do usunięcia</param>
        public static void DeleteVertexArray(this GlInterface gl, int count, int[] arrays)
        {
            gl.Ext().DeleteVertexArrays(count, arrays);
        }
        /// <summary>
        /// Metoda usuwająca pojedyńczą tablicę wierzchołków
        /// </summary>
        /// <param name="gl"></param>
        /// <param name="array">Id tablicy do usunięcia</param>
        public static void DeleteVertexArray(this GlInterface gl, int array)
        {
            gl.Ext().DeleteVertexArrays(1, new int[] {array});
        }
        /// <summary>
        /// Metoda do tworznia uniformu z pojedyńczym intem
        /// </summary>
        /// <param name="gl"></param>
        /// <param name="location">Localizacja uniformu</param>
        /// <param name="value">Wartość do ustawienia</param>
        public static void Uniform1i(this GlInterface gl, int location, int value)
        {
            gl.Ext().Uniform1i(location, value);
        }
        /// <summary>
        /// Metoda do tworzenia uniformu z 4 wartościami typu float
        /// </summary>
        /// <param name="gl"></param>
        /// <param name="location">Localizacja uniformu</param>
        /// <param name="v0">Pierwsza wartość</param>
        /// <param name="v1">Druga wartość</param>
        /// <param name="v2">Trzecia wartość</param>
        /// <param name="v3">Czwarta wartość</param>
        public static void Uniform4f(this GlInterface gl, int location, float v0, float v1, float v2, float v3)
        {
            gl.Ext().Uniform4f(location, v0, v1, v2, v3);
        }
        /// <summary>
        /// Metoda do tworzenia uniformu z 3 wartościami typu float
        /// </summary>
        /// <param name="gl"></param>
        /// <param name="location">Localizacja uniformu</param>
        /// <param name="v0">Pierwsza wartość</param>
        /// <param name="v1">Druga wartość</param>
        /// <param name="v2">Trzecia wartość</param>
        public static void Uniform3f(this GlInterface gl, int location, float v0, float v1, float v2)
        {
            gl.Ext().Uniform3f(location, v0, v1, v2);
        }
        /// <summary>
        /// Metoda generująca Id tekstury
        /// </summary>
        /// <param name="gl"></param>
        /// <returns>Id utworzonej tekstury</returns>
        public static int GenTexture(this GlInterface gl)
        {
            var rv = new int[1];
            gl.GenTextures(1, rv);
            return rv[0];
        }
        /// <summary>
        /// Metoda ustawiająca tryb renderowania poligonów
        /// </summary>
        /// <param name="gl"></param>
        /// <param name="face"></param>
        /// <param name="mode"></param>
        public static void PolygonMode(this GlInterface gl, int face, int mode)
        {
            gl.Ext().PolygonMode(face, mode);
        }
        /// <summary>
        /// Metoda ustawiająca funkcję blendowania
        /// </summary>
        /// <param name="gl"></param>
        /// <param name="sfactor"></param>
        /// <param name="dfactor"></param>
        public static void BlendFunc(this GlInterface gl, int sfactor, int dfactor)
        {
            gl.Ext().BlendFunc(sfactor, dfactor);
        }
        
        public static void BindBufferRange(this GlInterface gl, int target, int index, int buffer, IntPtr offset,
            IntPtr size)
        {
            gl.Ext().BindBufferRange(target, index, buffer, offset, size);
        }

        public static void BufferSubData(this GlInterface gl, int target, IntPtr offset, IntPtr size, IntPtr data)
        {
            gl.Ext().BufferSubData(target, offset, size, data);
        }

        public static void UniformBlockBinding(this GlInterface gl, int program, int uniformBlockIndex, int uniformBlockBinding)
        {
            gl.Ext().UniformBlockBinding(program, uniformBlockIndex, uniformBlockBinding);
        }

        public static int GetUniformBlockIndex(this GlInterface gl, int program, string uniformBlockName)
        {
            return gl.Ext().GetUniformBlockIndex(program, uniformBlockName);
        }
    }

    /// <summary>
    /// Klasa bindujaca brakujace funkcje OpenGl'a, które się nie znalazły w klasie GlInterface z używanego przez aplikację API
    /// </summary>
    public class GlInterfaceExtention : GlInterfaceBase<GlInterface.GlContextInfo>
    {
        public GlInterfaceExtention(Func<string, IntPtr> getProcAddress, GlInterface.GlContextInfo context) 
            : base(getProcAddress, context)
        {
        }

        public GlInterfaceExtention(Func<Utf8Buffer, IntPtr> nativeGetProcAddress, GlInterface.GlContextInfo context) 
            : base(nativeGetProcAddress, context)
        {
        }

        public GlInterfaceExtention(GlInterface gl) : base(gl.GetProcAddress, gl.ContextInfo)
        {
        }
        
        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate void GlBindVertexArray(int array);
        [GlEntryPoint("glBindVertexArray")]
        public GlBindVertexArray BindVertexArray { get; }
        
        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate void GlGenVertexArrays(int size, int[] arrays);
        [GlEntryPoint("glGenVertexArrays")]
        public GlGenVertexArrays GenVertexArrays { get; }

        public int GenVertexArray()
        {
            var rv = new int[1];
            GenVertexArrays(1, rv);
            return rv[0];
        }

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate void GlDeleteVertexArray(int count, int[] arrays);
        [GlEntryPoint("glDeleteVertexArrays")]
        public GlDeleteVertexArray DeleteVertexArrays { get; }
        
        
        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate void GlUniform1i(int location, int value);
        [GlEntryPoint("glUniform1i")]
        public GlUniform1i Uniform1i { get; }
        
        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate void GlUniform4f(int location, float v0, float v1, float v2, float v3);
        [GlEntryPoint("glUniform4f")]
        public GlUniform4f Uniform4f { get; }
        
        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate void GlUniform3f(int location, float v0, float v1, float v2);
        [GlEntryPoint("glUniform3f")]
        public GlUniform3f Uniform3f { get; }
        
        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate void GlPolygonMode(int face, int mode);
        [GlEntryPoint("glPolygonMode")]
        public GlPolygonMode PolygonMode { get; }
        
        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate void GlBlendFunc(int sfactor, int dfactor);
        [GlEntryPoint("glBlendFunc")]
        public GlBlendFunc BlendFunc { get; }
        
        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate void GlBindBufferRange(int target, int index, int buffer, IntPtr offset, IntPtr size);
        [GlEntryPoint("glBindBufferRange")]
        public GlBindBufferRange BindBufferRange { get; }
        
        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate void GlBufferSubData(int target, IntPtr offset, IntPtr size, IntPtr data);
        [GlEntryPoint("glBufferSubData")]
        public GlBufferSubData BufferSubData { get; }
        
        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate int GlGetUniformBlockIndex(int program, string uniformBlockName);
        [GlEntryPoint("glGetUniformBlockIndex")]
        public GlGetUniformBlockIndex GetUniformBlockIndex { get; }
        
        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate void GlUniformBlockBinding(int program, int uniformBlockIndex, int uniformBlockBinding);
        [GlEntryPoint("glUniformBlockBinding")]
        public GlUniformBlockBinding UniformBlockBinding { get; }

    }
}
